
import React, { Component } from 'react';
import './nav.css';
import {NavItem,Icon,Input} from 'react-materialize';
import { List, Avatar, Badge,Dropdown,Menu,Button,Form,Modal,message,Popover} from 'antd';
import "antd/dist/antd.css";
import axios from 'axios';
import { connect } from 'react-redux';
import {BASE_END_POINT} from '../../config/URL'
import {withRouter} from 'react-router-dom'
import {logout} from '../../actions/LogoutActions'
import  {allStrings} from '../../assets/strings'
import {ChangeLanguage} from '../../actions/LanguageAction'
import moment from 'moment'
import io from 'socket.io-client';
import Typing from 'react-typing-animation';



class Nav extends Component {
   page =1;
   MSGpages =1;
   peoplePages=1;

    state = {
      selectedImage:null,
      seen:false,
      currentFreindName:'',
      currentFriend:0,
      notifs:[],
      count:[],
      unseenCount:[],
      messages:[],
      modal1Visible: false,
      modal2Visible: false,
      friendMessages: [],
      once:false,
      showMessagesDialog:false,
      sentMSG:'',
      typing:false,
    }

    constructor(props){
      super(props)
      if(this.props.isRTL){
        allStrings.setLanguage('ar')
      }else{
        allStrings.setLanguage('en')
      }
    }

    componentDidUpdate(){
      //this.seenMessages()
      //this.getMessages(this.peoplePages)
    }
    componentDidMount(){
      this.getNotif()
      this.getCount()
      //this.seenMessages()
     // this.getMessages(1)
      this.getUnseenCount()

      setInterval(()=>{
        this.getNotif(true)
       this.getCount()
      },1000*60*3)

      this.socket = io("https://chatmoney.herokuapp.com/chat",{ query: `id=${this.props.currentUser.user.id}`} );  
      this.socket.on('connect', () => {
          //console.log("connect   " + this.socket.connected); // true
          //console.log("connect   " + this.socket.id);
          //console.log(this.socket);
          
        });

        this.socket.on('disconnect', () => {
         // console.log("disconnect  "+this.socket.disconnected); // true
        });

        this.socket.on('newMessage', (data) => {
          //console.log("newMessage   " + data); // true
          //console.log(data); // true
          this.setState({seen:false,friendMessages:this.state.currentFriend==data.user._id?[...this.state.friendMessages,data]:[...this.state.friendMessages],typing:false})
        });

        this.socket.on('seen', (data) => {
         // console.log("seen   " + data); // true
          //console.log(data); // true
          this.setState({seen:true})
          //this.getMessages()
         // this.getFriendMessages(this.state.currentFriend)
        });

        this.socket.on('stopTyping', (data) => {
          //console.log("StopTyping   " + data); // true
          //console.log(data); // true
          //this.getMessages()
          this.setState({typing:false})
        });


        this.socket.on('typing', (data) => {
         // console.log("typing   " + data); // true
          //console.log(data); // true
          this.setState({typing:true})
          //this.setState({friendMessages:[...this.state.friendMessages,data]})
        });

        this.socket.on('announcements', (data) => {
          //console.log("announcements   " );
          //console.log(data) // true
        });

        /*this.socket.emit("newAds",{
          adsType: 1,
          text: 'ADS1',
          userId: 1,
       } );

       this.socket.on('newAds', (data) => {
       
         console.log("ADS  ",data); // true
       
       });*/
       /*this.socket.emit("newInvite",{
        fromId: 9,
        toId: 4,
        roomId: 1,
     } );

     this.socket.on('newInvite', (data) => {
     
       console.log("ADS  ",data); // true
     
     });*/

     
     this.socket.on("globalMessage",(data) => {
   
      console.log("message  ",data); // true
    
    });

 

    }

    readNotification = (notID) => {
      //console.log(notID)
    axios.put(`${BASE_END_POINT}notif/${notID}/read`,{}, {
        headers: {
          'Content-Type': 'application/json',
          //this.props.currentUser.token
          'Authorization': `Bearer ${this.props.currentUser.token}`
        },
    }).then(Response=>{
        console.log("notification is red")
        console.log(Response)
        this.page=1;
        //this.setState({notifs:[]})
        //this.getNotif();
        this.getCount()
        
        //this.props.getUnreadNotificationsNumers(this.props.currentUser.token)
    }).catch(error=>{
        //console.log(error.response)
    })
}

    getNotif= (reload) => {
      this.page += 1
      axios.get(`${BASE_END_POINT}notif?page=${this.page-1}&limit=20`,{
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${this.props.currentUser.token}`
          },
        })
      .then(response=>{
        console.log("ALL notif   ",response.data)
        //console.log(response.data)
        this.setState({
          notifs: 
         reload? response.data.data : [...this.state.notifs , ...response.data.data ]   
          })
          this.page += 1
     
      })
      .catch(error=>{
        //console.log("ALL notif ERROR")
        //console.log(error.response)
      })
    }

    getMessages= (page,update) => {
      //${this.props.currentUser.user.id}
      axios.get(`${BASE_END_POINT}/messages/lastContacts?id=${this.props.currentUser.user.id}&&page=${page}&&limit=20`,{
          headers: {
            'Content-Type': 'application/json',
            //'Authorization': `Bearer ${this.props.currentUser.token}`
          },
        })
      .then(response=>{
        //console.log("ALL messages")
        //console.log(response.data)
        this.setState({messages:update?response.data.data:[...this.state.messages,...response.data.data]})
       /* const f = response.data.data
        let friends = []
        let ids = []
        for (var i = 0; i < f.length; i++) {
          let user = f[i]
          console.log("MSG Friend  ",user)
          if (!ids.includes(user.user._id) && user.user._id!=1) {
            friends.push(user)
            ids.push(user.user._id)
          }
        }

        this.setState({messages: friends})*/
              
      })
      .catch(error=>{
       // console.log("ALL messages ERROR")
        //console.log(error.response)
      })
    }

    validateTax = (rule, value, callback) => {
      const { form } = this.props;
      if ( isNaN(value) ) {
        callback('Please enter integer Value');
      } 
      else {
        callback();
      }
    };

    getCount= () => {
      axios.get(`${BASE_END_POINT}notif/unreadCount`,{
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${this.props.currentUser.token}`
          },
        })
      .then(response=>{
        //console.log("ALL notif")
        //console.log(response.data.data)
        this.setState({
          count: response.data
          })
        
      })
      .catch(error=>{
        //console.log("ALL notif ERROR")
        //console.log(error.response)
      })
    }
    getUnseenCount= () => {
      axios.get(`${BASE_END_POINT}messages/unseenCount`,{
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${this.props.currentUser.token}`
          },
        })
      .then(response=>{
       // console.log(response.data.data)
        this.setState({
          unseenCount: response.data
          })
        
      })
      .catch(error=>{
       // console.log(error.response)
      })
    }
    handleSubmit = (e) => {
      //
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
          if (!err) {
            //console.log('Received values of form: ', values);
            const data = {
              text:values.msg,
              image:'',
              user:{
                  _id:this.props.currentUser.user.id
              },
            }                      
          this.socket.emit("globalMessage",{
             //toId: this.state.currentFriend,
             data: data
          } );
            this.setModal1Visible(false)
           
      
          }
        });
        
      }
    
    setModal1Visible(modal1Visible) {
      this.setState({ modal1Visible });
    }
    setModal2Visible(modal2Visible) {
      this.setState({ modal2Visible });
    }


    getFriendMessages(friendID, page, refresh) {
      this.setState({loading:true,showMessagesDialog:true})
      axios.get(`${BASE_END_POINT}messages?userId=${this.props.currentUser.user.id}&&friendId=${friendID}&&page=${page}&&limit=20`)
          .then(response => {
             // console.log("MEssages")
              //console.log(response.data)
             // console.log(response)
              this.setState({loading:false,friendMessages:[...response.data.data.reverse(),...this.state.friendMessages]})
          }).catch(error => {
              this.setState({loading:false})
             // console.log("message error");
              //console.log(error.response);
              
          })
  
}

     menuMessage = () => {
       return (
        <List
        itemLayout="horizontal"
        dataSource={this.state.messages}
        onScroll={() => {
          //this.setState({oo:true})
          this.peoplePages++
          this.getMessages(this.peoplePages)
        }}
        renderItem={item => (
          <List.Item 
          style={{backgroundColor:item.from.id!=this.props.currentUser.user.id&&item.seen==false&&'#e1e1e1'}}
          onClick={() => {
            this.setState({currentFreindName:item.from.id!=this.props.currentUser.user.id?item.from.firstname+" "+item.from.lastname:item.to.firstname+" "+item.to.lastname, currentFriend:item.from.id!=this.props.currentUser.user.id?item.from.id:item.to.id})
            this.getFriendMessages(item.from.id!=this.props.currentUser.user.id?item.from.id:item.to.id,this.MSGpages)
          } }
          >
            <List.Item.Meta style={{padding: "10px"}} 
              avatar={<Avatar src={item.from.id!=this.props.currentUser.user.id?item.from.img:item.to.img} />}
              title={<a href="#">{item.from.id!=this.props.currentUser.user.id?item.from.firstname:item.to.firstname}</a>}
              description={item.content.length>=20?item.content.substring(0, 20):item.content}
            />
          
          <div>             
              <Icon  className='controller-icon' type="plus" />
              <span style={{marginTop: "20px" ,display: "inline-block"
              }}>{moment(item.from.createdAt).fromNow()}</span>
            </div>
          </List.Item>
          
          
        )}
       // footer={this.page>=3 && <Button onClick={() => this.getNotif()}>loading more</Button>}
        
      />
       )
     }

     seenMessages() {
      //friendId=${this.props.currentUser.user.type=='CLIENT'?this.props.data.salesMan.id:this.props.data.client.id}
      axios.put(`${BASE_END_POINT}messages?userId=${this.props.currentUser.user.id}&&friendId=${this.state.currentFriend}`)
          .then(response => {
             // console.log("Update Seen")
              //console.log(response.data)
              //console.log(response)
              //this.getMessages(this.page,false);
              //this.getMessages()
              //this.getFriendMessages(this.state.currentFriend)
              
          }).catch(error => {
              //this.setState({loading:false})
             // console.log("seen error");
              //console.log(error.response);
              if (!error.response) {
                  //this.setState({errorText:Strings.noConnection})
              }
          })
        }

        /*uploadImage= ()=>{
          var data = new FormData();
          data.append('file',this.state.selectedImage)
          data.append('upload_preset','jg6ir63c')
          axios.post(`https://api.cloudinary.com/v1_1/boody-car/image/upload`,data, {
          headers: {
            'Content-Type': 'application/json',
          },
        })
        .then(response=>{
          console.log("IMAGE RES")
          console.log(response.data.url)
        }).catch(error=>{
          console.log("IMAGE ERROR")
          console.log(error)
          console.log(error.response)
        })

        }*/



  render() {
    const { getFieldDecorator } = this.props.form;
    const {logout, currentUser, userToken,history} = this.props;
    //console.log(this.props.isRTL)

        const menu2 = (
          <Menu>
            <Menu.Item
            onClick={()=> this.props.history.push('/AdminInfo',{data:this.props.currentUser.user})}
             key="0">{allStrings.profile}</Menu.Item>
            <Menu.Divider />
             <Menu.Item 
            onClick={()=>{
              this.props.ChangeLanguage(true)
              allStrings.setLanguage('ar')
              localStorage.setItem('@lang','ar'); 
            }}
             key="2">{allStrings.arabic}</Menu.Item>
             <Menu.Item 
            onClick={()=>{
              this.props.ChangeLanguage(false)
              allStrings.setLanguage('en')
              localStorage.setItem('@lang','en');
            }}
             key="3">{allStrings.english}</Menu.Item>
             
              <Menu.Item 
            onClick={()=>logout(userToken, currentUser.token, history)}
             key="1">{allStrings.logout}</Menu.Item>
          </Menu>
        );
        const data = this.state.notifs.map(val=>[""+val.id,""+val.resource.firstname,""+val.resource.img,""+val.description,""+val.read])
      //console.log(data)
        const menu = (
        <List  onScroll={() => /*this.page <= 3 &&*/ this.getNotif()}
          itemLayout="horizontal"
          dataSource={this.state.notifs}
          renderItem={item => (
            <List.Item 
            onClick={()=>{
             
              if(item.read == false){
                this.readNotification(item.id)
              }
              
              /*if(item.description.toLowerCase().includes("new order")){
                this.props.history.push('/pendingOrder',{data:item})
              }*/
              this.props.history.push('/SalesManInfoNoti',{data:item.resource})

            }} >
              <List.Item.Meta style={{padding: "10px" , backgroundColor:!item.read&&'#e1e1e1'}} 
                avatar={<Avatar src={item.resource.img} />}
                title={<a href="#">{item.resource.username}</a>}
                description={item.arabicDescription}

              />
            </List.Item>
            
          )}
          /*footer={this.page>=3 && <Button onClick={() => this.getNotif()}>loading more</Button>}*/
          
        />
      );


    
    return (
      <nav>
        
        <Modal
                    title={
                      <div style={{height:50,width:300}}>
                        

                        <span style={{textAlign: "center",display: "inline-block",
                                    marginLeft: "71%",marginTop: "4%",fontSize: "20px"}}>
                                    {this.state.currentFreindName}
                        </span>
                        {this.state.typing&&
                          <div style={{marginLeft:50}}>
                          <Typing span={90} hideCursor>
                            <span>Typing...</span>
                          </Typing>
                          </div>
                        }
                   
                    
                      </div>
                    }
                    visible={this.state.showMessagesDialog}
                    onOk={this.handleSubmit}
                    okText={allStrings.ok}
                    
                    onCancel={() => { 
                      this.MSGpages=1                  
                      this.setState({showMessagesDialog:false,friendMessages:[]})
                      this.getUnseenCount()
                      this.getMessages(0,true)
                    }}
                    footer={null}
                  >
                    
                    <div class="chat"> 
                    <a onClick={()=>{
                          this.MSGpages++
                          this.getFriendMessages(this.state.currentFriend,this.MSGpages)
                        }} style={{display:"block",color:"#26a69a",textAlign:"center",marginBottom:" 25px"}}> + load more</a>
                      {this.state.friendMessages.map(msg=>(
                        msg.user._id==this.props.currentUser.user.id?
                        <div class="chat-message me">{msg.text}
                          <span class="distext">{moment(msg.createdAt).fromNow()}</span>
                          <br />
                          {msg.image&&
                          <img style={{width:200,height:300}} src={msg.image} alt="Logo" />
                          }
                          {/*<i class="material-icons seen" >done</i>*/}
                        </div>
                        :
                        <div class="chat-message them">{msg.text}
                          <span class="distextright">{moment(msg.createdAt).fromNow()}</span>
                          <br />
                          {msg.image&&
                          <img style={{width:200,height:300}} src={msg.image} alt="Logo" />
                          }
                        {/*<i class="material-icons seen">done</i>*/}
                      </div>
                      ))}
                          
                 
                    </div>

                    {this.state.seen&&
                    <div style={{marginTop:10,marginBottom:-15}}>                  
                      <span>{allStrings.seen}</span>                  
                    </div>
                    }

                    <div class="bottom">
                   
                    <input                  
                    onFocus={()=>{
                     // console.log('foucs')
                      this.socket.emit("seen",{
                        //toId: this.props.currentUser.user.type=='CLIENT'?this.props.data.salesMan.id:this.props.data.client.id ,
                        toId:this.state.currentFriend,
                        myId:this.props.currentUser.user.id
                     } );
                    }}
                   
                    value={this.state.sentMSG}
                    onChange={(val)=>{
                      this.setState({once:true,sentMSG:val.target.value})
                      if(!val.target.value || val.target.value.length==0){
                        //console.log("admin [aner not value")
                        this.socket.emit("stopTyping",{
                          toId:this.state.currentFriend,
                       } )
                      } 
                      if(val.target.value){
                        //console.log('typing')
                        this.socket.emit("typing",{
                          //toId: this.props.currentUser.user.type=='CLIENT'?this.props.data.salesMan.id:this.props.data.client.id ,
                          toId:this.state.currentFriend,
                          //myId:this.props.currentUser.user.id
                       } );
                      }

                      
                      
                    }}
                     placeholder="write your message" style={{  marginBottom: '6px',
                        marginTop: "10px",width:"77%"}}/>
                        <Button
                         onClick={()=>{
                             if(this.state.sentMSG || this.state.selectedImage){
                              if(this.state.selectedImage){
                                var data = new FormData();
                                data.append('file',this.state.selectedImage)
                                data.append('upload_preset','jg6ir63c')
                                axios.post(`https://api.cloudinary.com/v1_1/boody-car/image/upload`,data, {
                                headers: {
                                  'Content-Type': 'application/json',
                                },
                              })
                              .then(response=>{
                               // console.log("IMAGE RES")
                                //console.log(response.data.url)
                                const data = {
                                  text: this.state.sentMSG?this.state.sentMSG:'',
                                  image:response.data.url,
                                  user:{
                                      _id:this.props.currentUser.user.id
                                  },
                                }                      
                              this.socket.emit("newMessage",{
                                 toId: this.state.currentFriend,
                                 data: data
                              } );
      
                              this.setState({seen:false,once:false, friendMessages:[...this.state.friendMessages,data],sentMSG:'',selectedImage:null})
                              })
                              .catch(error=>{
                                //console.log("IMAGE ERROR")
                                //console.log(error)
                                //console.log(error.response)
                              })                      
                               }else{
                                const data = {
                                  text: this.state.sentMSG,
                                  image:'',
                                  user:{
                                      _id:this.props.currentUser.user.id
                                  },
                                }                      
                              this.socket.emit("newMessage",{
                                 toId: this.state.currentFriend,
                                 data: data
                              } );
      
                              this.setState({seen:false,once:false, friendMessages:[...this.state.friendMessages,data],sentMSG:'',selectedImage:null})
                               }
                              
                             }
                          /* if(this.state.sentMSG){                         
                          const data = {
                            text: this.state.sentMSG,
                            image:'',
                            user:{
                                _id:this.props.currentUser.user.id
                            },
                          }                      
                        this.socket.emit("newMessage",{
                           toId: this.state.currentFriend,
                           data: data
                        } );

                        this.setState({seen:false,once:false, friendMessages:[...this.state.friendMessages,data],sentMSG:''})
                      
                      }*/
                         }}
                         style={{width:"15%",marginTop: "25px",
                        float: "right",
                        background: "#26a69a",
                        color: "#fff"}}>Send</Button>
                        <div class="image-upload">
                            <label for="file-input">
                                <img src="http://lh3.googleusercontent.com/7rADIIAXkEedoq9sf6SO4ZiBXkpu-XQCJBxratH4Ij4D1LxH4NbvcdRPLmjdsxfG662h"/>
                            </label>
                            <input id="file-input" type="file"  multiple onChange= {(e)=>{
                          console.log("image  ",e.target.files[0])
                          this.setState({selectedImage:e.target.files[0]});
                        }}/>
                        </div>
                    </div>           
                    
                </Modal>
        <div class={`nav-wrapper ${!this.props.select&&'nav-wrapper-active'}`}>
          <ul class="right hide-on-med-and-down">
            <li>
            <Button  className='send'  onClick={() => this.setModal1Visible(true)}>{allStrings.sendMessageToAll}</Button>
              <Modal
                    title={allStrings.sendme}
                    visible={this.state.modal1Visible}
                    onOk={this.handleSubmit}
                    okText={allStrings.yes}
                    cancelText={allStrings.no}
                    onCancel={() => this.setModal1Visible(false)}
                  >
                    <Form onSubmit={this.handleSubmit} className="login-form">
                        <Form.Item>
                        {getFieldDecorator('msg', {
                            rules: [{ required: true, message: 'Please enter message' },
                  
                          ],
                        })(
                            <Input placeholder={allStrings.message} />
                        )}
                        </Form.Item>
                       
                    </Form>
                </Modal>
            </li>
            
            <li className='action'>
            <Dropdown overlay={menu} trigger={['click']}>
              <a className="ant-dropdown-link" href="#">
             <Icon>notifications</Icon><span class="badge red">{this.state.count.unread}</span>
              </a>
            </Dropdown>
            </li>
            {/*<li className='action'>
            <Dropdown overlay={this.menuMessage()} trigger={['click']}>
              <a className="ant-dropdown-link" href="#">
              <Icon>chat_bubble</Icon><span class="badge red">{this.state.unseenCount.unseen}</span>
              </a>
            </Dropdown>
            </li>
            */}
            <li className='action'>
            <Dropdown overlay={menu2} trigger={['click']}>
              <a className="ant-dropdown-link" href="#">
              <Icon>more_vert</Icon>
              </a>
            </Dropdown>
            
            </li>

          

            
            
          </ul>
        </div>
      </nav> 
    );
  }
}

const mapToStateProps = state => ({
  isRTL: state.lang.isRTL,
  currentUser: state.auth.currentUser,
  userToken: state.auth.userToken,
  select: state.menu.select
})

const mapDispatchToProps = {
  logout,
  ChangeLanguage
}

export default withRouter(connect(mapToStateProps,mapDispatchToProps)(Nav = Form.create({ name: 'normal_login' })(Nav)));

