import {LANG} from './types';

export function ChangeLanguage(change){
    return(dispatch) => {
        dispatch({type:LANG,payload:change})
    }
}