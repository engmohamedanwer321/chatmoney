import { message} from 'antd';
import "antd/dist/antd.css";
import axios from 'axios';
import {
  LOGIN_REQUEST,LOGIN_SUCCESS,USER_TOKEN,CURRENT_USER
   } from './types';
import { BASE_END_POINT } from '../config/URL';


//
export function login(username,password, token, history) {
 console.log(username,"    ",token,"    ",password)
  return (dispatch) => {    
    dispatch({ type: LOGIN_REQUEST });
    let l = message.loading('انتظر...', 2.5)
    axios.post(`${BASE_END_POINT}signin`, JSON.stringify({
      username:username,
      password:password,
      token:token
    }), {
      headers: {
        'Content-Type': 'application/json',
      },
    }).then(res => {
      
      console.log('login Done  ',res.data);  
        if(res.data.user.type=='ADMIN'){
          l.then(() => message.success('مرحبا', 2.5)) 
          localStorage.setItem('@QsathaUser', JSON.stringify(res.data));  
          dispatch({ type: LOGIN_SUCCESS, payload: res.data});  
            history.push('/Dashboard') 
        } else{
          l.then(() => message.error('هذا المستخدم غير مصرح لة بدخول لوحة التحكم', 2.5))
        }
    
    
    })
      .catch(error => {
        l.then(() => message.error('المستخدم غير مسجل فى قاعدة البيانات', 2.5))
        console.log('outer');
        console.log(error.response);
      });
  };
}

export function getUser(user){
  return dispatch => {
    dispatch({ type: CURRENT_USER, payload: user });
  }
}


export function userToken(token){
  //console.log("hi  "+token)
  return dispatch => {
    dispatch({type:USER_TOKEN,payload:token})
  }
}

/*export function userLocation(postion) {
  return dispatch => {
    console.log(postion)
    dispatch({type:USER_LOCATION,payload:postion})
  }
  
}
*/
