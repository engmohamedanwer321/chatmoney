import firebase from 'firebase';

export const initializeFirebase = () => {
    var config = {
      apiKey: "AIzaSyDRmT2iCB5-fOOgdS-0YIybZ8zozBEDRAo",
      authDomain: "geebwewady.firebaseapp.com",
      databaseURL: "https://geebwewady.firebaseio.com",
      projectId: "geebwewady",
      storageBucket: "",
      messagingSenderId: "595545465481",
      appId: "1:595545465481:web:1b1a0bd9198138bc"
      };
    firebase.initializeApp(config);
  }

  export const askForPermissioToReceiveNotifications = async () => {
    try {
  
      const messaging = firebase.messaging();
  
      await messaging.requestPermission();
      const token = await messaging.getToken();
      console.log('user token: ', token);
  
      return token;
    } catch (error) {
      console.error(error);
    }
  }

  



  
