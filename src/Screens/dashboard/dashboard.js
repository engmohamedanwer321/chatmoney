import React, { Component } from 'react';
import AppMenu from '../../components/menu/menu';
import Graph from '../../components/graph/graph';
import Nav from '../../components/navbar/navbar';
import Footer from '../../components/footer/footer';

import './dashboard.css';
import {Icon,Table} from 'react-materialize'
import axios from 'axios';
import {BASE_END_POINT} from '../../config/URL'
import {NavLink} from 'react-router-dom';
import { notification,Skeleton,Card, Avatar } from 'antd';
import 'antd/dist/antd.css';
import firebase from 'firebase';
import { connect } from 'react-redux';
import  {allStrings} from '../../assets/strings'
import {withRouter} from 'react-router-dom'
const { Meta } = Card;


class Dashboard extends Component {
   
  state = {
    users:[],
    counts:[],
    actions:[],
    loading:true,
    loading2:true,
   
}

constructor(props){
    super(props)
    if(this.props.isRTL){
      allStrings.setLanguage('ar')
    }else{
      allStrings.setLanguage('en')
    }

  }
  
 
    componentDidMount(){
       // console.log(this.props.currentUser)
        console.log("push   ",this.props.history)
        this.getCounts()
        this.getLastAction()
            this.getLastUsers()
            this.reciveNotification()
    }
      
    reciveNotification = async () => {
        try{
          const messaging = firebase.messaging();
          await messaging.requestPermission();
           await messaging.onMessage(msg=>{
              notification.open({
                  message: msg.notification.title,
                  description: msg.notification.body,
                  icon:  <Icon>group</Icon>,
                });          
         //     console.log('user token: ', msg)
           });
          
        }catch (error) {
          //console.error(error);
        }
    }
   
       
       //submit form
       //PENDING
       getCounts= () => {
         axios.get(`${BASE_END_POINT}admin/count`,{
            headers: {
              'Content-Type': 'application/json',
              'Authorization': `Bearer ${this.props.currentUser.token}`
            },
          })
         .then(response=>{
           console.log("ALL counts")
           console.log(response.data)
           this.setState({counts:response.data,loading:false})
         })
         .catch(error=>{
           console.log("ALL counts ERROR")
           console.log(error.response)
           console.log(error)
         })
       }
       getLastUsers= () => {
        axios.get(`${BASE_END_POINT}admin/users`,{
           headers: {
             'Content-Type': 'application/json',
             'Authorization': `Bearer ${this.props.currentUser.token}`
           },
         })
        .then(response=>{
          //console.log("ALL last users")
          //console.log(response.data)
          this.setState({users:response.data,loading2:false})
        })
        .catch(error=>{
          //console.log("ALL last users ERROR")
          //console.log(error.response)
        })
      }
      getLastAction= () => {
        axios.get(`${BASE_END_POINT}admin/actions`,{
           headers: {
             'Content-Type': 'application/json',
             'Authorization': `Bearer ${this.props.currentUser.token}`
           },
         })
        .then(response=>{
          //console.log("ALL last actions")
          //console.log(response.data)
          this.setState({actions:response.data,loading2:false})
        })
        .catch(error=>{
          //console.log("ALL last orders ERROR")
         // console.log(error.response)
        })
      }
        
       
  render() {
      const {counts} = this.state;
      const {users} = this.state;
      const {actions} = this.state;
      const loadingView = 
       <Skeleton  active/> 

       const{select,isRTL } = this.props
       //alert(''+window.innerHeight)
    return (
      <div >   
        <AppMenu height={'200%'} goTo={this.props.history}></AppMenu>
        <Nav></Nav>
        
        <div style={{marginRight:!select?'20.2%':'5.5%'}}>
          <div className='content' >

              <div className="row">
                  <div className="col s12 m6 xl4 l6 count1">
                  <NavLink to='/users'>
                  <div className="icons">
                      <Icon>group</Icon>
                  </div>
                  <div className='info' >
                      <p>{allStrings.user}</p>
                      <span>{this.state.loading?loadingView:counts.clients}</span>
                  </div>
                  </NavLink>
                  </div>          
                 

                  <div className="col s12 m6 xl4 l6 count2" >
                  <NavLink to='/Admins'>
                      <div className="icons">
                      <Icon>verified_user</Icon>
                      </div>
                      <div className='info'>
                          <p>{allStrings.admins}</p>
                          <span>{this.state.loading?loadingView:counts.admins}</span>
                      </div>
                  </NavLink>
                  </div>


                  <div className="col s12 m6 xl4 l6 count3">
                  <NavLink to='/Traders'>
                      <div className="icons">
                      <Icon>directions_walk</Icon> 
                      </div>
                      <div className='info'>
                          <p>{allStrings.traders}</p>
                          <span>{this.state.loading?loadingView:counts.tradersCount}</span>
                      </div>
                  </NavLink>
                  </div> 
                
              </div>
              
              <div className="row">
                  <div className="col s12 m6 xl4 l6 count1">
                  <NavLink to='/users'>
                  <div className="icons">
                      <Icon>mail_outline</Icon>
                  </div>
                  <div className='info' >
                      <p>{allStrings.messages}</p>
                      <span>{this.state.loading?loadingView:counts.messages}</span>
                  </div>
                  </NavLink>
                  </div>          
                 

                  <div className="col s12 m6 xl4 l6 count2" >
                  <NavLink to='/sales-man'>
                      <div className="icons">
                      <Icon>chat_bubble</Icon>
                      </div>
                      <div className='info'>
                          <p>{allStrings.room}</p>
                          <span>{this.state.loading?loadingView:""+counts.rooms}</span>
                      </div>
                  </NavLink>
                  </div>


                  <div className="col s12 m6 xl4 l6 count3">
                  <NavLink to='/category'>
                      <div className="icons">
                      <Icon>storefront</Icon> 
                      </div>
                      <div className='info'>
                          <p>{allStrings.gifts}</p>
                          <span>{this.state.loading?loadingView:counts.gifts}</span>
                      </div>
                  </NavLink>
                  </div> 
                
              </div>

              <div className="row">
                  <div className="col s12 m6 xl4 l6 count1">
                  <NavLink to='/users'>
                  <div className="icons">
                      <Icon>storefront</Icon>
                  </div>
                  <div className='info' >
                      <p>{allStrings.giftsTransfer}</p>
                      <span>{this.state.loading?loadingView:counts.giftsTransfer}</span>
                  </div>
                  </NavLink>
                  </div>          
                 

                  <div className="col s12 m6 xl4 l6 count2" >
                  <NavLink to='/sales-man'>
                      <div className="icons">
                      <Icon>report_problem</Icon>
                      </div>
                      <div className='info'>
                          <p>{allStrings.problems}</p>
                          <span>{this.state.loading?loadingView:counts.problems}</span>
                      </div>
                  </NavLink>
                  </div>


                  <div className="col s12 m6 xl4 l6 count3">
                  <NavLink to='/category'>
                      <div className="icons">
                      <Icon>message</Icon> 
                      </div>
                      <div className='info'>
                          <p>{allStrings.chatMessages}</p>
                          <span>{this.state.loading?loadingView:counts.chatMessages}</span>
                      </div>
                  </NavLink>
                  </div> 
                
              </div>

              <div className="row">
                  <div className="col s12 m6 xl4 l6 count1">
                  <NavLink to='/users'>
                  <div className="icons">
                      <Icon>supervised_user_circle</Icon>
                  </div>
                  <div className='info' >
                      <p>{allStrings.allUsers}</p>
                      <span>{this.state.loading?loadingView:counts.allUsers}</span>
                  </div>
                  </NavLink>
                  </div>          
                 

                  <div className="col s12 m6 xl4 l6 count2" >
                  <NavLink to='/sales-man'>
                      <div className="icons">
                      <Icon>local_atm</Icon>
                      </div>
                      <div className='info'>
                          <p>{allStrings.money}</p>
                          <span>{this.state.loading?loadingView:counts.money}</span>
                      </div>
                  </NavLink>
                  </div>


                  
                
              </div>

          </div>
          </div>
      </div>
    );
  }
}

const mapToStateProps = state => ({
    isRTL: state.lang.isRTL,
    currentUser: state.auth.currentUser,
    select: state.menu.select,

  })
  
  const mapDispatchToProps = {
    
  }

export default withRouter( connect(mapToStateProps,mapDispatchToProps) (Dashboard) );
