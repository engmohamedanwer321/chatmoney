import React, { Component } from 'react';
import Dashboard from './dashboard/dashboard';
import Users from './users/users';
import Category from './category/category';


import Shops from './shops/shops';
import Terms from './terms/terms';
import Report from './report/report';
import Login from './login/login';
import Contactus from './contactus/contactus'


import AdminInfo from './admin info/admin info'

import UserInfo from './user info/user info'
import Splash from './splash/splash'
import CategoryInfo from './category info/category info'
import ShopInfo from './shop info/shop info'

import { Route, BrowserRouter, Switch } from 'react-router-dom'
import {askForPermissioToReceiveNotifications} from '../config/push-notification';
import { Provider } from 'react-redux'
import {store,persistor } from '../store';
import { PersistGate } from 'redux-persist/integration/react'

import Admins from './admins/admins'
import AdminsInfo from './admins info/admins info'
import Traders from './traders/traders'
import TradersInfo from './traders info/traders info'
import About from './about/about';
import privacy from './privacy/privacy'
import Conditions from './conditions/conditions'
import Rank from './rank/rank'
import RankInfo from './rank info/rank info'
import Title from './title/title'
import TitleInfo from './title info/title info'
import AdsType from './ads type/ads type'
import AdsInfo from './ads info/ads info'
import Market from './market/market'
import MarketInfo from './market info/market info'
import Prize from './prize/prize'
import PrizeInfo from './prize info/prize info'
import Gift from './gift/gift'
import GiftInfo from './gift info/gift info'
import Avatar from './avatar/avatar'
import AvatarInfo  from './avatar info/avatar info'
import WelcomeMessage from './welcome message/welcome message'
import WelcomeMessageInfo from './welcome message info/welcome message info'
import Intro from './intro/intro'
import IntroInfo from './intro info/intro info'
import Room from './room/room'
import RoomInfo from './room info/room info'
import Problem from './problem/problem'
import ProblemInfo from './problem info/problem info'
import CoinHistory from './coin history/coin history'
import BlockUsers from './block users/block users'
import Winners from './winners/winners'
import WinnerInfo from './winner info/winner info'

class App extends Component {
  componentDidMount(){
   // askForPermissioToReceiveNotifications()
  }
  //
  render() {
    return (
      <Provider store={store}>
         <PersistGate loading={null} persistor={persistor}>
      <BrowserRouter>
        <div className="App">
          <Switch>
            <Route path='/Dashboard' component={Dashboard}/>
            <Route path='/CoinHistory' component={CoinHistory}/>
            <Route path='/WinnerInfo' component={WinnerInfo}/>
            <Route path='/Winners' component={Winners}/>
            <Route path='/BlockUsers' component={BlockUsers}/>
            <Route  path='/Conditions' component={Conditions}/>
            <Route  path='/Problem' component={Problem}/>
            <Route  path='/ProblemInfo' component={ProblemInfo}/>
            <Route  path='/Room' component={Room}/>
            <Route  path='/RoomInfo' component={RoomInfo}/>
            <Route  path='/Intro' component={Intro}/>
            <Route  path='/IntroInfo' component={IntroInfo}/>
            <Route  path='/WelcomeMessage' component={WelcomeMessage}/>
            <Route  path='/WelcomeMessageInfo' component={WelcomeMessageInfo}/>
            <Route  path='/Avatar' component={Avatar}/>
            <Route  path='/AvatarInfo' component={AvatarInfo}/>
            <Route  path='/Gift' component={Gift}/>
            <Route  path='/GiftInfo' component={GiftInfo}/>
            <Route  path='/Prize' component={Prize}/>
            <Route  path='/PrizeInfo' component={PrizeInfo}/>
            <Route  path='/Market' component={Market}/>
            <Route  path='/MarketInfo' component={MarketInfo}/>
            <Route  path='/AdsType' component={AdsType}/>
            <Route  path='/AdsInfo' component={AdsInfo}/>
            <Route  path='/Rank' component={Rank}/>
            <Route  path='/Title' component={Title}/>
            <Route  path='/TitleInfo' component={TitleInfo}/>
            <Route  path='/RankInfo' component={RankInfo}/>
            <Route  path='/Login' component={Login}/>
            <Route  path='/Admins' component={Admins}/>
            <Route  path='/AdminsInfo' component={AdminsInfo}/>
            <Route  path='/Traders' component={Traders}/>
            <Route  path='/TradersInfo' component={TradersInfo}/>
            <Route  path='/about' component={About}/>
            <Route  path='/privacy' component={privacy}/>
            
      
            <Route path='/Contactus' component={Contactus}/>
            <Route exact path='/' component={Splash}/>
            <Route path='/users' component={Users} />
            
           
            <Route path='/terms' component={Terms} />
            <Route path='/category' component={Category} />
            <Route path='/shops' component={Shops} />
            <Route path='/reports' component={Report} />
            <Route path='/AdminInfo' component={AdminInfo} />
            <Route path='/UserInfo' component={UserInfo} />
            <Route path='/CategoryInfo' component={CategoryInfo} />
            <Route path='/ShopInfo' component={ShopInfo} />
 

          </Switch>
        </div>
      </BrowserRouter>
      </PersistGate>
      </Provider>
    );
  }
}

export default App;
