import React from 'react';
import Menu from '../../components/menu/menu';
import AppMenu from '../../components/menu/menu';

import Nav from '../../components/navbar/navbar';
import Tables from '../../components/table/table';
import Footer from '../../components/footer/footer';

import './prize.css';
import { Skeleton, message,Modal, Form, Icon, Input, Button,Popconfirm,Select} from 'antd';
import "antd/dist/antd.css";
import axios from 'axios';
import {BASE_END_POINT} from '../../config/URL'
import { connect } from 'react-redux';
import  {allStrings} from '../../assets/strings'


class Prize extends React.Component {
  page = 0;
  pagentationPage=0;
  counter=0;
  state = {
    modal1Visible: false,
    confirmDelete: false,
    selectedCategory:null,
     categories:[],
     file:null,
     loading:true,
     tablePage:0,
     }

     constructor(props){
      super(props)
      if(this.props.isRTL){
        allStrings.setLanguage('ar')
      }else{
        allStrings.setLanguage('en')
      }
    }

     onChange = (e) => {
      this.setState({file:e.target.files[0]});
     
  }
    //submit form
    flag = -1;
    getCategories = (page,deleteRow) => {
      axios.get(`${BASE_END_POINT}prize?page=${page}&limit=20`)
      .then(response=>{
        //console.log("ALL Categories")
        //console.log(response.data)
        if(deleteRow){
          this.setState({tablePage:0})
         }
        this.setState({categories:deleteRow?response.data.data:[...this.state.categories,...response.data.data],loading:false})
      })
      .catch(error=>{
        //console.log("ALL Categories ERROR")
        //console.log(error.response)
        this.setState({loading:false})
      })
    }
    componentDidMount(){
      this.getCategories(1,true)
    }
    OKBUTTON = (e) => {
      this.deleteCategory()
     }

     deleteCategory = () => {
       let l = message.loading(allStrings.wait, 2.5)
       axios.delete(`${BASE_END_POINT}prize/${this.state.selectedCategory}`,{
         headers: {
           'Content-Type': 'application/json',
           'Authorization': `Bearer ${this.props.currentUser.token}`
         },
       })
       .then(response=>{
           l.then(() => message.success(allStrings.deleteDone, 2.5))
           this.getCategories(1,true)
           this.flag = -1
       })
       .catch(error=>{
           console.log('delete error   ', error.response)
           l.then(() => message.error('Error', 2.5))
       })
    }
    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
          if (!err) {
            const data={
              description:values.description,
              link:values.link,
              price:values.price,
             
            }
           
            let l = message.loading(allStrings.wait, 2.5)
            axios.post(`${BASE_END_POINT}prize`,JSON.stringify(data),{
              headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${this.props.currentUser.token}`
              },
            })
            .then(response=>{
                l.then(() => message.success(allStrings.addDone, 2.5));
                this.setState({ modal1Visible:false,file:null });
                
                this.getCategories(1,true)
                this.flag = -1
                this.props.form.resetFields()
            })
            .catch(error=>{
              //  console.log(error.response)
                l.then(() => message.error(error.response.data.errors[0].msg, 2.5))
            })
          }
        });
        
      }
     
    //end submit form

      setModal1Visible(modal1Visible) {
        this.setState({ modal1Visible });
      }

//end modal
    render() {
        //form
         const { getFieldDecorator } = this.props.form;
         //select
         const Option = Select.Option;

         function handleChange(value) {
            //console.log(value); 
         }
         //end select
         //upload props
         const props = {
            name: 'file',
            action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
            headers: {
              authorization: 'authorization-text',
            },
            onChange(info) {
              if (info.file.status !== 'uploading') {
                //console.log(info.file, info.fileList);
              }
              if (info.file.status === 'done') {
                message.success(`${info.file.name} file uploaded successfully`);
              } else if (info.file.status === 'error') {
                message.error(`${info.file.name} file upload failed.`);
              }
            },
          };
          //end upload props
          let controls = (
            <Popconfirm
            title={allStrings.areYouSure}
            onConfirm={this.OKBUTTON}
            onCancel={this.fCANCELBUTTON}
            okText={allStrings.ok}
            cancelText={allStrings.cancel}
          >
             <Icon className='controller-icon' type="delete" />
          </Popconfirm>
         )
        
          let list =this.state.categories.map((val,index)=>[
            val.id,val.price,val.description,val.link,controls
          ])

          const loadingView = [
            [<Skeleton  active/> ],
            [<Skeleton active/> ],
            [<Skeleton  active/> ],
            [<Skeleton active/> ],
            
           ]
           const {select} = this.props;
      return (
          <div>
              <AppMenu goTo={this.props.history}></AppMenu>
              <Nav></Nav>
              <div style={{marginRight:!select?'20.2%':'5.5%',borderWidth:2}}>
              <Tables
               columns={
                 this.state.loading?['loading...']:
                  [allStrings.id,allStrings.price,allStrings.description,allStrings.link, allStrings.remove]
                } 
              title={allStrings.prizes}
              page={this.state.tablePage}
              onCellClick={(colData,cellMeta,)=>{
                //console.log('col index  '+cellMeta.colIndex)
                //console.log('row index   '+colData)
                if(cellMeta.colIndex!=4){
                  
                  //console.log(this.state.categories[cellMeta.rowIndex])
                  this.props.history.push('/PrizeInfo',{data:this.state.categories[this.pagentationPage+cellMeta.rowIndex]})
                }else if(cellMeta.colIndex==4){
                    const id = list[this.pagentationPage+cellMeta.rowIndex][0];
                    this.setState({selectedCategory:id})
                    //console.log(id)
                  }
              }}
               onChangePage={(currentPage)=>{
                this.setState({tablePage:currentPage})
                if(currentPage>this.counter){
                  this.counter=currentPage;
                  this.pagentationPage=this.pagentationPage+10
                }else{
                 this.counter=currentPage;
                 this.pagentationPage=this.pagentationPage-10
                }
                //console.log(currentPage)
                if(currentPage%2!=0  && currentPage > this.flag){
                  this.getCategories(currentPage+1)
                  this.flag  = currentPage;
                 
                }
                  
              }}
              arr={this.state.loading?loadingView:list}></Tables>
              <div>
              <Button style={{color: 'white', backgroundColor:'#25272e', marginLeft:60}} onClick={() => this.setModal1Visible(true)}>{allStrings.addPrize}</Button>
              <Modal
                    title={allStrings.add}
                    visible={this.state.modal1Visible}
                    onOk={this.handleSubmit}
                    onCancel={() => this.setModal1Visible(false)}
                    okText={allStrings.ok}
                    cancelText={allStrings.cancel}
                  >
                    <Form onSubmit={this.handleSubmit} className="login-form">
                        <Form.Item>
                        {getFieldDecorator('link', {
                            rules: [{ required: true, message: 'Please enter link' }],
                        })(
                            <Input placeholder={allStrings.link}/>
                        )}
                        </Form.Item>

                        <Form.Item>
                        {getFieldDecorator('description', {
                            rules: [{ required: true, message: 'Please enter description' }],
                        })(
                            <Input placeholder={allStrings.description}/>
                        )}
                        </Form.Item>

                       
                        <Form.Item>
                        {getFieldDecorator('price', {
                            rules: [{ required: true, message: 'Please enter rank price' }],
                        })(
                            <Input placeholder={allStrings.price}/>
                        )}
                        </Form.Item>

                    </Form>
                </Modal>
             
              </div> 
            </div>
            
          </div>
      );
    }
  }

  const mapToStateProps = state => ({
    isRTL: state.lang.isRTL,
    currentUser: state.auth.currentUser,
    select: state.menu.select,

  })
  
  const mapDispatchToProps = {
  }

  export default  connect(mapToStateProps,mapDispatchToProps)(Prize = Form.create({ name: 'normal_login' })(Prize));
