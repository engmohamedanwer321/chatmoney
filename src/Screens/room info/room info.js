import React from 'react';
import AppMenu from '../../components/menu/menu';
import Nav from '../../components/navbar/navbar';
import Footer from '../../components/footer/footer';

import './room info.css';
import {Carousel} from 'react-materialize';
import "antd/dist/antd.css";
 import axios from 'axios';
import {BASE_END_POINT} from '../../config/URL'
import { connect } from 'react-redux';
import  {allStrings} from '../../assets/strings'
import { Upload, message,Modal, Form, Icon, Input, Button,Select} from 'antd';
import {Table} from 'react-materialize'

class RoomInfo extends React.Component {
    state = {
        modal1Visible: false,
         category:this.props.location.state.data,
         file: this.props.location.state.data.img,
         }

         constructor(props){
          super(props)
          if(this.props.isRTL){
            allStrings.setLanguage('ar')
          }else{
            allStrings.setLanguage('en')
          }
        }
        
         onChange = (e) => {
            this.setState({file:e.target.files[0]});
        }
        
         componentDidMount()
         {
             //console.log("ANWEr")
             //console.log(this.props.currentUser)
            
             //this.props.location.state.data
             //console.log(this.props.location.state.data)
         }
    
    deleteCategory = () => {
        let l = message.loading(allStrings.wait, 2.5)
        axios.delete(`${BASE_END_POINT}room/${this.state.category.id}`,{
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${this.props.currentUser.token}`
          },
        })
        .then(response=>{
            l.then(() => message.success(allStrings.deleteDone, 2.5))
            this.props.history.goBack()

        })
        .catch(error=>{
            console.log(error.response)
            l.then(() => message.error('Error', 2.5))
        })
     }

     openRoom = () => {
      let l = message.loading(allStrings.wait, 2.5)
      axios.put(`${BASE_END_POINT}room/${this.state.category.id}/openRoom`,{},{
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${this.props.currentUser.token}`
        },
      })
      .then(response=>{
          l.then(() => message.success(allStrings.open, 2.5))
          this.props.history.goBack()

      })
      .catch(error=>{
          console.log(error.response)
          l.then(() => message.error('Error', 2.5))
      })
   }

   closeRoom = () => {
    let l = message.loading(allStrings.wait, 2.5)
    axios.put(`${BASE_END_POINT}room/${this.state.category.id}/closeRoom`,{},{
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${this.props.currentUser.token}`
      },
    })
    .then(response=>{
        l.then(() => message.success(allStrings.open, 2.5))
        this.props.history.goBack()

    })
    .catch(error=>{
        console.log(error.response)
        l.then(() => message.error('Error', 2.5))
    })
 }


 
    render() {
        const { getFieldDecorator } = this.props.form;
         //select
         const Option = Select.Option;
         const {category} = this.state

         function handleChange(value) {
            //console.log(value); 
         }
         //end select
        //upload props
        const props = {
          name: 'file',
          action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
          headers: {
            authorization: 'authorization-text',
          },
          onChange(info) {
            if (info.file.status !== 'uploading') {
              //console.log(info.file, info.fileList);
            }
            if (info.file.status === 'done') {
              message.success(`${info.file.name} file uploaded successfully`);
            } else if (info.file.status === 'error') {
              message.error(`${info.file.name} file upload failed.`);
            }
          },
        };

        //end upload props
        const {select} = this.props;
      return (
          
        <div>
         <AppMenu height={'150%'} goTo={this.props.history}></AppMenu>
          <Nav></Nav>
          <div style={{marginRight:!select?'20.2%':'5.5%'}}>
        <div className='login'>
        <div class="row">
            <div class="col m2">
                <div class='title' style={{backgroundColor:'#001529'}} >
                    <h2 class="center-align" style={{color:'#fff'}}>{allStrings.roomInfo}</h2>
                </div>
                
                <div class="row" style={{marginTop:"0px"}}>
                    <form class="col s12">
                        <div class="row">
                            <div class="input-field col s6">
                            <input id="name" type="text" class="validate" disabled value={category.id}>
                            </input>
                            <label for="name" class="active">{allStrings.id}</label>
                            </div>

                            <div class="input-field col s6">
                            <input id="name" type="text" class="validate" disabled value={category.name}>
                            </input>
                            <label for="name" class="active">{allStrings.name}</label>
                            </div>

                            <div class="input-field col s6">
                            <input id="name" type="text" class="validate" disabled value={category.msgCount}>
                            </input>
                            <label for="name" class="active">{allStrings.messageCountL}</label>
                            </div>

                        </div>

                        <div class="row">
                            <div class="input-field col s6">
                            <input id="name" type="text" class="validate" disabled value={category.type}>
                            </input>
                            <label for="name" class="active">{allStrings.type}</label>
                            </div>

                            <div class="input-field col s6">
                            <input id="name" type="text" class="validate" disabled value={category.rate}>
                            </input>
                            <label for="name" class="active">{allStrings.rate}</label>
                            </div>

                        </div>

                        <div class="row">
                            <div class="input-field col s6">
                            <input id="name" type="text" class="validate" disabled value={category.open}>
                            </input>
                            <label for="name" class="active">{allStrings.open}</label>
                            </div>

                            <div class="input-field col s6">
                            <input id="name" type="text" class="validate" disabled value={category.file}>
                            </input>
                            <label for="name" class="active">{allStrings.file}</label>
                            </div>

                        </div>

                        <div class="row">
                            <div class="input-field col s6">
                            <input id="name" type="text" class="validate" disabled value={category.maleCount}>
                            </input>
                            <label for="name" class="active">{allStrings.maleCount}</label>
                            </div>

                            <div class="input-field col s6">
                            <input id="name" type="text" class="validate" disabled value={category.femaleCount}>
                            </input>
                            <label for="name" class="active">{allStrings.femalCountL}</label>
                            </div>

                            <div class="input-field col s6">
                            <input id="name" type="text" class="validate" disabled value={category.main}>
                            </input>
                            <label for="name" class="active">{allStrings.main}</label>
                            </div>

                        </div>

                        <div className='dash-table'>
                            <h5>{allStrings.founder} : </h5>
                            <div className='row'>
                                <div className="col s6 m6 l6 dashboard-table">
                                    <Table>
                                        <thead>
                                            <tr>
                                            <th data-field="id">{allStrings.id}</th>
                                            <th data-field="title">{allStrings.name}</th>
                                            <th data-field="price">{allStrings.email}</th>
                                            <th data-field="price">{allStrings.phone}</th>
                                            
                                           
                                            </tr>
                                        </thead>  

                                        <tbody>
                                        <tr onClick={()=>{this.props.history.push('/UserInfo',{data:category.founder}) }} >
                                                <td>{category.founder.id}</td>
                                                <td>{category.founder.username}</td>
                                                <td>{category.founder.email}</td>
                                                <td>{category.founder.phone}</td>
                                        </tr>

                                        </tbody>
                                    </Table>
                                </div>
                            </div>
                        </div> 
                        

                        <div className='dash-table'>
                            <h5>{allStrings.maneger} : </h5>
                            <div className='row'>
                                <div className="col s6 m6 l6 dashboard-table">
                                    <Table>
                                        <thead>
                                            <tr>
                                            <th data-field="id">{allStrings.id}</th>
                                            <th data-field="title">{allStrings.name}</th>
                                            <th data-field="price">{allStrings.email}</th>
                                            <th data-field="price">{allStrings.phone}</th>
                                            
                                           
                                            </tr>
                                        </thead>  

                                        <tbody>
                                        {category.manager.map(val=>(
                                           <tr onClick={()=>{this.props.history.push('/UserInfo',{data:category.founder}) }} >
                                           <td>{val.id}</td>
                                           <td>{val.username}</td>
                                           <td>{val.email}</td>
                                           <td>{val.phone}</td>
                                   </tr>
                                        ))}

                                        </tbody>
                                    </Table>
                                </div>
                            </div>
                        </div> 

                        <div className='dash-table'>
                            <h5>{allStrings.users} : </h5>
                            <div className='row'>
                                <div className="col s6 m6 l6 dashboard-table">
                                    <Table>
                                        <thead>
                                            <tr>
                                            <th data-field="id">{allStrings.id}</th>
                                            <th data-field="title">{allStrings.name}</th>
                                            <th data-field="price">{allStrings.email}</th>
                                            <th data-field="price">{allStrings.phone}</th>
                                            
                                           
                                            </tr>
                                        </thead>  

                                        <tbody>
                                        {category.users.map(val=>(
                                           <tr onClick={()=>{this.props.history.push('/UserInfo',{data:category.founder}) }} >
                                           <td>{val.id}</td>
                                           <td>{val.username}</td>
                                           <td>{val.email}</td>
                                           <td>{val.phone}</td>
                                   </tr>
                                        ))}

                                        </tbody>
                                    </Table>
                                </div>
                            </div>
                        </div> 
      
                            <a class="waves-effect waves-light btn btn-large delete " onClick={this.deleteCategory}><i class="material-icons left spcial">delete</i>{allStrings.remove}</a>
                            <a class="waves-effect waves-light btn btn-large edit " onClick={this.openRoom}>{allStrings.open2}</a>
                            <a class="waves-effect waves-light btn btn-large edit " onClick={this.closeRoom}>{allStrings.close}</a>
                           


                            <div>
                           
                        
                            </div>
                        </form>
                        
                    </div>
            </div>
        </div>
        </div>
        <Footer></Footer>
        </div>
    </div>
      );
    }
  }

  const mapToStateProps = state => ({
    isRTL: state.lang.isRTL,
    currentUser: state.auth.currentUser,
    select: state.menu.select,
  })
  
  const mapDispatchToProps = {
  }

export default connect(mapToStateProps,mapDispatchToProps)(RoomInfo = Form.create({ name: 'normal_login', })(RoomInfo)) ;
