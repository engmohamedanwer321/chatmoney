import React from 'react';
import AppMenu from '../../components/menu/menu';
import Nav from '../../components/navbar/navbar';
import Footer from '../../components/footer/footer';

import './category info.css';
import {Carousel} from 'react-materialize';
import "antd/dist/antd.css";
 import axios from 'axios';
import {BASE_END_POINT} from '../../config/URL'
import { connect } from 'react-redux';
import  {allStrings} from '../../assets/strings'
import { Upload, message,Modal, Form, Icon, Input, Button,Select} from 'antd';

class CategoryInfo extends React.Component {
    state = {
        modal1Visible: false,
         category:this.props.location.state.data,
         flag:this.props.location.state.flag,
         file: this.props.location.state.data.img,
         years: [],
         }

         constructor(props){
          super(props)
          if(this.props.isRTL){
            allStrings.setLanguage('ar')
          }else{
            allStrings.setLanguage('en')
          }
        }

        getYears = () => {
          axios.get(`${BASE_END_POINT}year`)
          .then(response=>{
            this.setState({years:response.data.data})
          })
          .catch(error=>{
            //console.log("ALL Categories ERROR")
            //console.log(error.response)
          })
        }
        
         onChange = (e) => {
            this.setState({file:e.target.files[0]});
        }
        
         componentDidMount()
         {
             this.getYears()
             //console.log("ANWEr")
             //console.log(this.props.currentUser)
            
             //this.props.location.state.data
             //console.log(this.props.location.state.data)
         }

    addSerVice = (service) => {
      let uri = '';
      if(service){
        uri = `${BASE_END_POINT}categories/${this.state.category.id}/service`
      }else{
        uri = `${BASE_END_POINT}categories/${this.state.category.id}/unservice`
      }
      let l = message.loading(allStrings.wait, 2.5)
      axios.put(uri,null,{
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${this.props.currentUser.token}`
        },
      })
      .then(response=>{
        l.then(() => message.success(allStrings.done, 2.5))
        this.props.history.goBack()
    })
    .catch(error=>{
       console.log( 'service error    ',  error.response)
        l.then(() => message.error('Error', 2.5))
    })

    }     
    
    deleteCategory = () => {
        let l = message.loading(allStrings.wait, 2.5)
        axios.delete(`${BASE_END_POINT}categories/${this.state.category.id}`,{
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${this.props.currentUser.token}`
          },
        })
        .then(response=>{
            l.then(() => message.success(allStrings.deleteDone, 2.5))
            this.props.history.goBack()

        })
        .catch(error=>{
            console.log(error.response)
            l.then(() => message.error('Error', 2.5))
        })
     }

     handleSubmit = (e) => {
      e.preventDefault();
      this.props.form.validateFields((err, values) => {
        if (!err) {
          //console.log('Received values of form: ', values);
          var form = new FormData();
          if(this.state.file){
            form.append('img', this.state.file);
          }
          
          form.append('categoryname', values.categoryname);
          form.append('arabicname', values.arabicname);
          form.append('type', values.type);
          form.append('arabicType', values.arabictype);
          
          if(this.state.flag){
            form.append('description', values.description);
          form.append('arabicDescription', values.arabicDescription);
            form.append('services', true)
            form.append('modal', values.modal.key);
          }
          let l = message.loading(allStrings.wait, 2.5)
          axios.put(`${BASE_END_POINT}categories/${this.state.category.id}`,form,{
            headers: {
              'Content-Type': 'application/x-www-form-urlencoded',
              'Authorization': `Bearer ${this.props.currentUser.token}`
            },
          })
          .then(response=>{
              l.then(() => message.success(allStrings.updatedDone, 2.5))
              this.props.history.goBack()
          })
          .catch(error=>{
           //   console.log(error.response)
              l.then(() => message.error(error.response.data.errors[0].msg, 2.5))
          })
        }
      });
      
    }

      
      //modal
     
      setModal1Visible(modal1Visible) {
        this.setState({ modal1Visible });
      }
    render() {
        const { getFieldDecorator } = this.props.form;
         //select
         const Option = Select.Option;
         const {category} = this.state

         function handleChange(value) {
            //console.log(value); 
         }
         //end select
        //upload props
        const props = {
          name: 'file',
          action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
          headers: {
            authorization: 'authorization-text',
          },
          onChange(info) {
            if (info.file.status !== 'uploading') {
              //console.log(info.file, info.fileList);
            }
            if (info.file.status === 'done') {
              message.success(`${info.file.name} file uploaded successfully`);
            } else if (info.file.status === 'error') {
              message.error(`${info.file.name} file upload failed.`);
            }
          },
        };

        //end upload props
        const {select} = this.props;
      return (
          
        <div>
         <AppMenu height={'150%'} goTo={this.props.history}></AppMenu>
          <Nav></Nav>
          <div style={{marginRight:!select?'20.2%':'5.5%'}}>
        <div className='login'>
        <div class="row">
            <div class="col m2">
                <div class='title' style={{backgroundColor:'#001529'}} >
                    <h2 class="center-align" style={{color:'#fff',marginTop:"10px"}}>{allStrings.categoryInfo}</h2>
                </div>
                <div class="row">
                <Carousel images={category.img} />
                </div>
                <div class="row" style={{marginTop:"0px"}}>
                    <form class="col s12">
                        <div class="row">
                            <div class="input-field col s6">
                            <input id="name" type="text" class="validate" disabled value={category.categoryname}>
                            </input>
                            <label for="name" class="active">{allStrings.EnglishCategoryName}</label>
                            </div>

                            <div class="input-field col s6">
                            <input id="name" type="text" class="validate" disabled value={category.arabicname}>
                            </input>
                            <label for="name" class="active">{allStrings.ArabicCategoryName}</label>
                            </div>

                        </div>


                        <div class="row">
                            <div class="input-field col s6">
                            <input id="name" type="text" class="validate" disabled value={category.type}>
                            </input>
                            <label for="name" class="active">{allStrings.EnglishCategoryType}</label>
                            </div>

                            <div class="input-field col s6">
                            <input id="name" type="text" class="validate" disabled value={category.arabicType}>
                            </input>
                            <label for="name" class="active">{allStrings.ArabicCategoryType}</label>
                            </div>

                        </div>

                        {this.state.flag&&
                        <div class="row">
                            <div class="input-field col s6">
                            <input id="name" type="text" class="validate" disabled value={category.description}>
                            </input>
                            <label for="name" class="active">{allStrings.englishDescribtion}</label>
                            </div>

                            <div class="input-field col s6">
                            <input id="name" type="text" class="validate" disabled value={category.arabicDescription}>
                            </input>
                            <label for="name" class="active">{allStrings.arabicDescribtion}</label>
                            </div>

                        </div>
                        }
      

                        <div class="row">
                            <div class="input-field col s6">
                            <input id="name" type="text" class="validate" disabled value={category.id}>
                            </input>
                            <label for="name" class="active">{allStrings.id}</label>
                            </div>

                            <div class="input-field col s6">
                            <input id="name" type="text" class="validate" disabled value={category.services}>
                            </input>
                            <label for="name" class="active">{allStrings.services}</label>
                            </div>
                        </div>

                        {this.state.flag&&
                        <div class="row">
                            <div class="input-field col s6">
                            <input id="name" type="text" class="validate" disabled value={category.modal}>
                            </input>
                            <label for="name" class="active">{allStrings.manufacturingYear}</label>
                            </div>                     
                        </div>
                        }
                        
                       
                            <a class="waves-effect waves-light btn btn-large delete " onClick={this.deleteCategory}><i class="material-icons left spcial">delete</i>{allStrings.remove}</a>
                            <a class="waves-effect waves-light btn btn-large edit" onClick={() => this.setModal1Visible(true)}><i class="material-icons left spcial">edit</i>{allStrings.edit}</a>

                            <a class="waves-effect waves-light btn btn-large edit" onClick={() => this.addSerVice(true)}><i class="material-icons left spcial">add</i>{allStrings.addService}</a>
                            <a class="waves-effect waves-light btn btn-large edit" onClick={() => this.addSerVice(false)}><i class="material-icons left spcial">remove</i>{allStrings.removerService}</a>


                            <div>
                            <Modal
                                title={allStrings.edit}
                                visible={this.state.modal1Visible}
                                onOk={this.handleSubmit}
                                okText={allStrings.ok}
                                cancelText={allStrings.cancel}
                                onCancel={() => this.setModal1Visible(false)}
                              >
                              
                               <Form onSubmit={this.handleSubmit} className="login-form">

                               <label for="name" class="lab">{allStrings.EnglishCategoryName}</label>
                                  <Form.Item>
                                  {getFieldDecorator('categoryname', {
                                      rules: [{ required: true, message: 'Please enter category name by english' }],
                                      initialValue: category.categoryname
                                  })(
                                      <Input  />
                                  )}
                                  </Form.Item>

                                  <label for="name" class="lab">{allStrings.ArabicCategoryName}</label>
                                  <Form.Item>
                                  {getFieldDecorator('arabicname', {
                                      rules: [{ required: true, message: 'Please enter category name by arabic' }],
                                      initialValue: category.arabicname
                                  })(
                                      <Input  />
                                  )}
                                  </Form.Item>
                           
                                  <label for="name" class="lab">{allStrings.EnglishCategoryType}</label>
                                  <Form.Item>
                                  {getFieldDecorator('type', {
                                      rules: [{ required: true, message: 'Please enter category type by english' }],
                                      initialValue: category.type
                                  })(
                                      <Input />
                                  )}
                                  </Form.Item>
                                  
                                  <label for="name" class="lab">{allStrings.ArabicCategoryType}</label>
                                 {  <Form.Item>
                                  {getFieldDecorator('arabictype', {
                                      rules: [{ required: true, message: 'Please enter category type by arabic' }],
                                      initialValue: category.arabicType
                                  })(
                                      <Input  />
                                  )}
                                  </Form.Item>
                                  }

                                {this.state.flag&&
                                  <label for="name" class="lab">{allStrings.englishDescribtion}</label>
                                }

                                {this.state.flag&&
                                <Form.Item>
                                  {getFieldDecorator('description', {
                                      rules: [{ required: true, message: 'Please enter english description' }],
                                      initialValue: category.description
                                  })(
                                      <Input />
                                  )}
                                  </Form.Item>
                                }

                                {this.state.flag&&
                                  <label for="name" class="lab">{allStrings.arabicDescribtion}</label>
                                } 
                                  {this.state.flag&&
                                  <Form.Item>
                                  {getFieldDecorator('arabicDescription', {
                                      rules: [{ required: true, message: 'Please enter Arabic Description' }],
                                      initialValue: category.arabicDescription
                                  })(
                                      <Input/>
                                  )}
                                  </Form.Item>
                                  }

                                  {this.state.flag&&
                                   <Form.Item>
                                   {getFieldDecorator('modal', {
                                       rules: [{ required: true, message: 'Please enter  modal' }],
                                   })(
                                     <Select labelInValue  
                                     placeholder={allStrings.manufacturingYear}
                                     style={{ width: '100%'}} onChange={handleChange}>
                                         {this.state.years.map(val=>
                                         <Option value={val.Year}>{val.Year}</Option>
                                         )}                 
                                     </Select>
                                   )}
                                   </Form.Item>
                                  }
                       
                                 
                              </Form>
                              <input class='file' type="file" onChange= {this.onChange} multiple></input>
                            </Modal>

                                
                        
                            </div>
                        </form>
                        
                    </div>
            </div>
        </div>
        </div>
        <Footer></Footer>
        </div>
    </div>
      );
    }
  }

  const mapToStateProps = state => ({
    isRTL: state.lang.isRTL,
    currentUser: state.auth.currentUser,
    select: state.menu.select,
  })
  
  const mapDispatchToProps = {
  }

export default connect(mapToStateProps,mapDispatchToProps)(CategoryInfo = Form.create({ name: 'normal_login', })(CategoryInfo)) ;
