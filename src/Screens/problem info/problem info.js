import React from 'react';
import AppMenu from '../../components/menu/menu';
import Nav from '../../components/navbar/navbar';
import Footer from '../../components/footer/footer';

import './problem info.css';
import {Carousel} from 'react-materialize';
import "antd/dist/antd.css";
 import axios from 'axios';
import {BASE_END_POINT} from '../../config/URL'
import { connect } from 'react-redux';
import  {allStrings} from '../../assets/strings'
import { Upload, message,Modal, Form, Icon, Input, Button,Select} from 'antd';
import {Table} from 'react-materialize'

class ProblemInfo extends React.Component {
    state = {
        modal1Visible: false,
         category:this.props.location.state.data,
         file:null,
         }

         constructor(props){
          super(props)
          if(this.props.isRTL){
            allStrings.setLanguage('ar')
          }else{
            allStrings.setLanguage('en')
          }
        }
        
         onChange = (e) => {
            this.setState({file:e.target.files[0]});
        }
        
         componentDidMount()
         {
             //console.log("ANWEr")
             //console.log(this.props.currentUser)
            
             //this.props.location.state.data
             //console.log(this.props.location.state.data)
         }
    
    deleteCategory = () => {
        let l = message.loading(allStrings.wait, 2.5)
        axios.delete(`${BASE_END_POINT}problem/${this.state.category.id}`,{
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${this.props.currentUser.token}`
          },
        })
        .then(response=>{
            l.then(() => message.success(allStrings.deleteDone, 2.5))
            this.props.history.goBack()

        })
        .catch(error=>{
            console.log(error.response)
            l.then(() => message.error('Error', 2.5))
        })
     }

     handleSubmit = (e) => {
      e.preventDefault();
      this.props.form.validateFields((err, values) => {
        if (!err) {
        
          let l = message.loading(allStrings.wait, 2.5)
          axios.put(`${BASE_END_POINT}problem/${this.state.category.id}/reply`,JSON.stringify({
            replyText:values.replyText
          }),{
            headers: {
              'Content-Type': 'application/json',
              'Authorization': `Bearer ${this.props.currentUser.token}`
            },
          })
          .then(response=>{
              l.then(() => message.success(allStrings.updatedDone, 2.5))
              this.props.history.goBack()
          })
          .catch(error=>{
           //   console.log(error.response)
              l.then(() => message.error(error.response.data.errors[0].msg, 2.5))
          })
        }
      });
      
    }

      
      //modal
      //
     
      setModal1Visible(modal1Visible) {
        this.setState({ modal1Visible });
      }
    render() {
        const { getFieldDecorator } = this.props.form;
         //select
         const Option = Select.Option;
         const {category} = this.state

         function handleChange(value) {
            //console.log(value); 
         }
         //end select
        //upload props
        const props = {
          name: 'file',
          action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
          headers: {
            authorization: 'authorization-text',
          },
          onChange(info) {
            if (info.file.status !== 'uploading') {
              //console.log(info.file, info.fileList);
            }
            if (info.file.status === 'done') {
              message.success(`${info.file.name} file uploaded successfully`);
            } else if (info.file.status === 'error') {
              message.error(`${info.file.name} file upload failed.`);
            }
          },
        };

        //end upload props
        const {select} = this.props;
      return (
          
        <div>
         <AppMenu height={'150%'} goTo={this.props.history}></AppMenu>
          <Nav></Nav>
          <div style={{marginRight:!select?'20.2%':'5.5%'}}>
        <div className='login'>
        <div class="row">
            <div class="col m2">
                <div class='title' style={{backgroundColor:'#001529'}} >
                    <h2 class="center-align" style={{color:'#fff'}}>{allStrings.problemInfo}</h2>
                </div>
                <div className='row'>
                <Carousel images={category.img} />
                </div>
                
                <div class="row" style={{marginTop:"0px"}}>
                    <form class="col s12">
                        <div class="row">
                            <div class="input-field col s6">
                            <input id="name" type="text" class="validate" disabled value={category.id}>
                            </input>
                            <label for="name" class="active">{allStrings.id}</label>
                            </div>

                            <div class="input-field col s6">
                            <input id="name" type="text" class="validate" disabled value={category.problemType}>
                            </input>
                            <label for="name" class="active">{allStrings.problemType}</label>
                            </div>

                        </div>

                        <div class="row">
                            <div class="input-field col s6">
                            <input id="name" type="text" class="validate" disabled value={category.description}>
                            </input>
                            <label for="name" class="active">{allStrings.description}</label>
                            </div>

                            <div class="input-field col s6">
                            <input id="name" type="text" class="validate" disabled value={""+category.replyText}>
                            </input>
                            <label for="name" class="active">{allStrings.replay}</label>
                            </div>

                        </div>

                        <div className='dash-table'>
                            <h5>{allStrings.sender} : </h5>
                            <div className='row'>
                                <div className="col s6 m6 l6 dashboard-table">
                                    <Table>
                                        <thead>
                                            <tr>
                                            <th data-field="id">{allStrings.id}</th>
                                            <th data-field="title">{allStrings.name}</th>
                                            <th data-field="price">{allStrings.email}</th>
                                            <th data-field="price">{allStrings.phone}</th>
                                            
                                           
                                            </tr>
                                        </thead>  

                                        <tbody>
                                        <tr onClick={()=>{this.props.history.push('/UserInfo',{data:category.user}) }} >
                                           <td>{category.user.id}</td>
                                           <td>{category.user.username}</td>
                                           <td>{category.user.email}</td>
                                           <td>{category.user.phone}</td>
                                        </tr>
                                        </tbody>
                                    </Table>
                                </div>
                            </div>
                        </div> 

                        {'message' in category&&
                        <div className='dash-table'>
                        <h5>{allStrings.message} : </h5>
                        <div className='row'>
                            <div className="col s6 m6 l6 dashboard-table">
                                <Table>
                                    <thead>
                                        <tr>
                                        <th data-field="id">{allStrings.id}</th>
                                        <th data-field="title">{allStrings.name}</th>
                                        <th data-field="price">{allStrings.email}</th>
                                        <th data-field="price">{allStrings.phone}</th>
                                        <th data-field="price">{allStrings.message}</th>
                                        
                                       
                                        </tr>
                                    </thead>  

                                    <tbody>
                                    <tr onClick={()=>{this.props.history.push('/UserInfo',{data:category.message.from}) }} >
                                       <td>{category.message.from.id}</td>
                                       <td>{category.message.from.username}</td>
                                       <td>{category.message.from.email}</td>
                                       <td>{category.message.from.phone}</td>
                                       <td>{category.message.content}</td>
                                    </tr>
                                    </tbody>
                                </Table>
                            </div>
                        </div>
                    </div> 
                      }
                       

      
                            <a class="waves-effect waves-light btn btn-large delete " onClick={this.deleteCategory}><i class="material-icons left spcial">delete</i>{allStrings.remove}</a>
                            <a class="waves-effect waves-light btn btn-large edit" onClick={() => this.setModal1Visible(true)}><i class="material-icons left spcial">edit</i>{allStrings.replay}</a>


                            <div>
                            <Modal
                                title={allStrings.edit}
                                visible={this.state.modal1Visible}
                                onOk={this.handleSubmit}
                                okText={allStrings.ok}
                                cancelText={allStrings.cancel}
                                onCancel={() => this.setModal1Visible(false)}
                              >
                              
                               <Form onSubmit={this.handleSubmit} className="login-form">
                               
                               <label for="name" class="lab">{allStrings.replay}</label>
                                  <Form.Item>
                                  {getFieldDecorator('replyText', {
                                      rules: [{ required: true, message: 'Please enter reply' }],
                                      
                                      
                                  })(
                                      <Input/>
                                  )}
                                  </Form.Item>

                                 
                                 
                              </Form>
                             
                            </Modal>

                                
                        
                            </div>
                        </form>
                        
                    </div>
            </div>
        </div>
        </div>
        <Footer></Footer>
        </div>
    </div>
      );
    }
  }

  const mapToStateProps = state => ({
    isRTL: state.lang.isRTL,
    currentUser: state.auth.currentUser,
    select: state.menu.select,
  })
  
  const mapDispatchToProps = {
  }

export default connect(mapToStateProps,mapDispatchToProps)(ProblemInfo = Form.create({ name: 'normal_login', })(ProblemInfo)) ;
