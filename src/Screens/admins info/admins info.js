import React, { Component } from 'react';
import Menu from '../../components/menu/menu';
import AppMenu from '../../components/menu/menu';
import Nav from '../../components/navbar/navbar';
import Footer from '../../components/footer/footer';

import './admins info.css';
import {Icon,Button,Card,Input,CardTitle} from 'react-materialize';
import "antd/dist/antd.css";
 import { Modal,Form ,Select ,DatePicker,message} from 'antd';
 import axios from 'axios';
 import {BASE_END_POINT} from '../../config/URL'
import {Table} from 'react-materialize'
import { connect } from 'react-redux'
import  {allStrings} from '../../assets/strings'
import {getUser} from '../../actions/AuthActions'



class AdminsInfo extends React.Component {
       //submit form
       page=1;
       type=null;
       state = {
        modal1Visible: false,
        user:this.props.location.state.data,
        file:null,
        ordermodal:false,
        orders:[],
        loading:false,
        selectFlag:false,
        showColorPicker:false,
        color:this.props.location.state.data.color,
        showNameColor:false,
        nameColor:this.props.location.state.data.nameColor,

        //this.props.location.state.data.img[0],
        
      }

      constructor(props){
        super(props)
        if(this.props.isRTL){
          allStrings.setLanguage('ar')
        }else{
          allStrings.setLanguage('en')
        }
      }

      getOrders= (page,type,reload) => {
        console.log("page  ",page)
        axios.get(`${BASE_END_POINT}orders?client=${this.state.user.id}&status=${type}&page=${page}&limit={20}`)
        .then(response=>{
          console.log("ALL orders")
          console.log(response.data.data)
          this.setState({selectFlag:true, orders:reload?response.data.data : [...this.state.orders,...response.data.data],loading:false})
        })
        .catch(error=>{
          console.log("ALL orders ERROR")
          console.log(error.response)
          this.setState({loading:false})
        })
      }

      onChange = (e) => {
        this.setState({file:e.target.files[0]});
    }

       componentDidMount()
       {
           //this.getOrders(this.page)
       }

       deleteUser = () => {
        let l = message.loading(allStrings.wait, 2.5)
        axios.delete(`${BASE_END_POINT}${this.state.user.id}/delete`,{
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Authorization': `Bearer ${this.props.currentUser.token}`
          },
        })
        .then(response=>{
            l.then(() => message.success(allStrings.deleteDone, 2.5))
            this.props.history.goBack()
        })
        .catch(error=>{
            //console.log(error.response)
            l.then(() => message.error('Error', 2.5))
        })
     }


       block = (active) => {
           let uri ='';
           if(active){
            uri = `${BASE_END_POINT}${this.state.user.id}/block`
           }else{
            uri = `${BASE_END_POINT}${this.state.user.id}/unblock`
           }
          let l = message.loading(allStrings.wait, 2.5)
           axios.put(uri,{},{
            headers: {
              'Content-Type': 'application/json',
              'Authorization': `Bearer ${this.props.currentUser.token}`
            },
          })
           .then(response=>{
               // console.log('done')
                if(active){
                    
                    l.then(() => message.success(allStrings.unblockDone, 2.5))
                    
                }else{
                
                   l.then(() => message.success(allStrings.blockDone, 2.5))
                }
                this.props.history.goBack()
           })
           .catch(error=>{
           // console.log('Error')
           // console.log(error.response)
            l.then(() => message.error('Error', 2.5))
           })
       }

     

       handleSubmit = (e) => {
        e.preventDefault();
        console.log('user ID     ',this.state.user.id)
        this.props.form.validateFields((err, values) => {
          if (!err) {
            console.log('Received values of form: ', values);
            var data = new FormData();
            if(this.state.file){
              data.append('img',this.state.file);
            }
            data.append("username",values.username)
            data.append("email",values.email)
            data.append("phone",values.phone)
            data.append("country",values.country)
            data.append("city",values.city)

            data.append("titleName",values.titleName)
            data.append("gender",values.gender.key)
            data.append("type",values.type.key)
            data.append("socialStatus",values.socialStatus.key)
            let l = message.loading(allStrings.wait, 2.5)
            axios.put(`${BASE_END_POINT}user/${this.state.user.id}/updateInfo`,data,{
              headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${this.props.currentUser.token}`
              },
            })
            .then(response=>{
                l.then(() => message.success(allStrings.updatedDone, 2.5));
               
                console.log("update      ",response.data)
                this.setState({ modal1Visible:false });
                this.props.history.goBack()
            })
            .catch(error=>{
                console.log(error.response)
                l.then(() => message.error(error.response.data.errors[0].msg, 2.5))
            })
          }
        });
       
        
      }

    //end submit form
      
      //modal
    
  
      setModal1Visible(modal1Visible) {
        this.setState({ modal1Visible });
      }
  

  //end modal
  
    render() {
        const { getFieldDecorator } = this.props.form;
        const {user} = this.state;
         //select
         const Option = Select.Option;

         function handleChange(value) {
            //console.log(value); 
         }
         //end select
         const {select} = this.props;
 
      return (
          
        <div>
         <AppMenu height={'200%'} goTo={this.props.history} />
        <Nav></Nav>
        <div style={{marginRight:!select?'20.2%':'5.5%',borderWidth:2}}>
        <div className='login'>
        <div class="row">
            <div class="col m2">
                <div class='title' style={{backgroundColor:'#001529'}}>
                    <h2 class="center-align" style={{color:'#fff'}}>{allStrings.adminInfo}</h2>
                </div>
                <div class="row">
                
                    <form class="col s12">
                    <img style={{borderColor:'#25272e'}} src={ 'img' in user?user.img:"https://theimag.org/wp-content/uploads/2015/01/user-icon-png-person-user-profile-icon-20.png"}></img>
                        <div class="row">
                            <div class="input-field col s6">
                            <input id="firstname" type="text" class="validate" disabled value={user.id}>
                            </input>
                            <label for="firstname" class="active">{allStrings.id}</label>
                            </div>
                            <div class="input-field col s6">
                            <input id="email" type="text" class="validate" disabled value={user.username}>
                            </input>
                            <label for="email" class="active">{allStrings.name}</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s6">
                            <input id="email" type="text" class="validate" disabled value={user.titleName}>
                            </input>
                            <label for="email" class="active">{allStrings.titleName}</label>
                            </div>
                            <div class="input-field col s6">
                            <input id="phone" type="text" class="validate" disabled value={user.email}></input>
                            <label for="phone" class="active">{allStrings.email}</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s6">
                            <input id="cardNum" type="text" class="validate" disabled value={user.phone}>
                            </input>
                            <label for="cardNum" class="active">{allStrings.phone}</label>
                            </div>

                            <div class="input-field col s6">
                            <input id="cardNum" type="text" class="validate" disabled value={user.type}>
                            </input>
                            <label for="cardNum" class="active">{allStrings.type}</label>
                            </div>
                            
                        </div>
                        <div class="row">
                            <div class="input-field col s6">
                            <input id="type" type="text" class="validate" disabled value={user.gender}>
                            </input>
                            <label for="type" class="active">{allStrings.gender}</label>
                            </div>
                            <div class="input-field col s6">
                            <input id="are" type="text" class="validate" disabled value={user.socialStatus}></input>
                            <label for="area" class="active">{allStrings.socialStatus}</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s6">
                            <input id="cardNum" type="text" class="validate" disabled value={user.balance}>
                            </input>
                            <label for="cardNum" class="active">{allStrings.balance}</label>
                            </div>
                            <div class="input-field col s6">
                            <input id="Address" type="text" class="validate" disabled value={user.msgNumber}></input>
                            <label for="Address" class="active">{allStrings.msgNumber}</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s6">                       
                             <div  style={{marginTop:10 ,width:30,height:30,borderRadius:15,backgroundColor:user.color }} ></div>                 
                            <label for="cardNum" class="active">{allStrings.color}</label>
                            </div>

                            <div class="input-field col s6">
                             <div  style={{marginTop:10 ,width:30,height:30,borderRadius:15,backgroundColor:user.nameColor }} ></div>
                            <label for="Address" class="active">{allStrings.nameColor}</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s6">
                            <input id="cardNum" type="text" class="validate" disabled value={user.block}>
                            </input>
                            <label for="cardNum" class="active">{allStrings.block}</label>
                            </div>

                            <div class="input-field col s6">
                            <input id="cardNum" type="text" class="validate" disabled value={""+user.verifycode}>
                            </input>
                            <label for="cardNum" class="active">{allStrings.verifycode}</label>
                            </div>
                           
                        </div>

    

                               
                        <div>
                        <a class="waves-effect waves-light btn btn-large delete"  onClick={()=>this.deleteUser()} ><i class="spcial material-icons left">delete</i>{allStrings.remove}</a>
                        <a class="waves-effect waves-light btn btn-large edit" onClick={() => this.block(true)}><i class="material-icons left spcial">block</i>{allStrings.block}</a>
                        <a class="waves-effect waves-light btn btn-large edit" onClick={() => this.block(false)}><i class="material-icons left spcial">remove</i>{allStrings.unblock}</a>                 
                        <a class="waves-effect waves-light btn btn-large edit" onClick={() => this.setModal1Visible(true)}><i class=" spcial material-icons left">edit</i>{allStrings.edit}</a>
                        </div>
                        
                                                                             
                        </form>
                        
                    <Modal
                            title="Edit"
                            visible={this.state.modal1Visible}
                            onOk={this.handleSubmit}
                            okText={allStrings.ok}
                            cancelText={allStrings.cancel}
                            onCancel={() => this.setModal1Visible(false)}
                        >

                        <div style={{marginBottom:20 ,textAlign:'center'}}>
                            <label for="name" class="lab">{allStrings.personalImage}</label>
                            <br/>
                            <input className='profileImg' type="file" onChange= {this.onChange}></input>
                        </div>
                            
                            <Form onSubmit={this.handleSubmit} className="login-form">

                             <label for="name" class="lab">{allStrings.name}</label>
                            <Form.Item>
                            {getFieldDecorator('username', {
                                rules: [{ required: true, message: 'Please enter username' }],
                                initialValue: user.username,
                            })(
                                <Input/>
                            )}
                            </Form.Item>

                            <label for="name" class="lab">{allStrings.titleName}</label>
                            <Form.Item>
                            {getFieldDecorator('titleName', {
                                rules: [{ required: true, message: 'Please enter title name' }],
                                initialValue: user.titleName,
                            })(
                                <Input/>
                            )}
                            </Form.Item>

                            <label for="name" class="lab">{allStrings.email}</label>
                            <Form.Item>
                            {getFieldDecorator('email', {
                                rules: [{ required: true, message: 'Please enter email' }],
                                initialValue:user.email
                            })(
                                <Input />
                            )}
                            </Form.Item>
                        
                            <label for="name" class="lab">{allStrings.phone}</label>
                            <Form.Item>
                            {getFieldDecorator('phone', {
                                rules: [{ required: true, message: 'Please enter phone' }],
                                initialValue:user.phone
                            })(
                                <Input/>
                            )}
                            </Form.Item>

                            <label for="name" class="lab">{allStrings.country}</label>
                            <Form.Item>
                            {getFieldDecorator('country', {
                                rules: [{ required: true, message: 'Please enter country' }],
                                initialValue:user.country
                            })(
                                <Input/>
                            )}
                            </Form.Item>

                            <label for="name" class="lab">{allStrings.city}</label>
                            <Form.Item>
                            {getFieldDecorator('city', {
                                rules: [{ required: true, message: 'Please enter city' }],
                                initialValue:user.city
                            })(
                                <Input/>
                            )}
                            </Form.Item>

                            <label for="name" class="lab">{allStrings.gender}</label>
                            <Form.Item>
                            {getFieldDecorator('gender', {
                              rules: [{ required: true, message: 'Please enter gender' }],
                          })(
                              <Select labelInValue
                              value={user.gender}  
                              placeholder={allStrings.gender}
                              style={{ width: '100%'}} onChange={handleChange}>
                                  <Option value="male">{allStrings.male}</Option>
                                  <Option value="female">{allStrings.female}</Option>
                              </Select>
                          )}
                          </Form.Item>

                          <label for="name" class="lab">{allStrings.type}</label>
                          <Form.Item>
                            {getFieldDecorator('type', {
                              rules: [{ required: true, message: 'Please enter type' }],
                          })(
                              <Select labelInValue 
                              value={user.type} 
                              placeholder={allStrings.type}
                              style={{ width: '100%'}} onChange={handleChange}>
                                  <Option value="CLIENT">{allStrings.user}</Option>
                                  <Option value="TRADER">{allStrings.trader}</Option>
                                  <Option value="ADMIN">{allStrings.admin}</Option>
                              </Select>
                          )}
                          </Form.Item>

                          <label for="name" class="lab">{allStrings.socialStatus}</label>
                          <Form.Item>
                            {getFieldDecorator('socialStatus', {
                              rules: [{ required: true, message: 'Please enter social status' }],
                              //initialValue:0,
                            })(
                              <Select labelInValue  
                              placeholder={allStrings.socialStatus}
                              style={{ width: '100%'}} onChange={handleChange}>
                                  <Option value="Single">{allStrings.single}</Option>
                                  <Option value="Married">{allStrings.married}</Option>
                              </Select>
                          )}
                          </Form.Item>

                            <label for="name" class="lab">{allStrings.balance}</label>
                            <Form.Item>
                            {getFieldDecorator('balance', {
                                rules: [{ required: true, message: 'Please enter balacne' }],
                                initialValue:user.balance
                            })(
                                <Input />
                            )}
                            </Form.Item>
         
                            </Form>
                    
                           
                        </Modal>

                        <Modal
                            title={allStrings.orders}
                            visible={this.state.ordermodal}                          
                            okText={allStrings.ok}
                            cancelText={allStrings.cancel}
                            onCancel={() => this.setState({ordermodal:false})}
                        >

                          

                          <div className='orderTypes'>
                            <button onClick={()=>{
                              this.getOrders(1,'PENDING',true)
                              this.page=1;
                              this.type='PENDING'
                            }}
                            >{allStrings.pendingOrder}</button>

                            <button onClick={()=>{
                              this.getOrders(1,'CANCEL',true)
                              this.page=1;
                              this.type='CANCEL'
                            }}
                            >{allStrings.canceledOrder}</button>

                            <button
                            onClick={()=>{
                              this.getOrders(1,'ON_PROGRESS',true)
                              this.page=1;
                              this.type='ON_PROGRESS'
                            }}
                            >{allStrings.onprogressOrder}</button>

                            <button
                            onClick={()=>{
                              this.getOrders(1,'DELIVERED',true)
                              this.page=1;
                              this.type='DELIVERED'
                            }}
                            >{allStrings.deliveredOrder}</button>

                            

                          </div>
                          {this.state.selectFlag?
                          <table>
                            <tr  className='tableHeader'>
                              <th>{allStrings.name}</th>
                              <th>{allStrings.phone}</th>
                              <th>{allStrings.order}</th>
                              <th>{allStrings.time}</th>
                              <th>{allStrings.shopName}</th>
                              {this.type!='PENDING'&&this.type!='CANCEL'&&
                              <th>{allStrings.salesmanName}</th>
                              }
                              {this.type!='PENDING'&&this.type!='CANCEL'&&
                              <th>{allStrings.salesmanPhone}</th>
                              }
                              {this.type!='PENDING'&&this.type!='CANCEL'&&
                              <th>{allStrings.offerPrice}</th>
                              }
                            </tr>
                            
                            {this.state.orders.map(order=>
                            <tr onClick={()=>{
                              this.props.history.push('/OrderInfo',{data:order})
                            }} className='tableContent'>
                              <th>{order.client.username}</th>
                              <th>{order.client.phone}</th>
                              <th>{order.description}</th>
                              <th>{order.time}</th>
                              <th>{order.shopName}</th>
                              {this.type!='PENDING'&&this.type!='CANCEL'&&
                              <th>{order.salesMan.username}</th>
                              }
                              {this.type!='PENDING'&&this.type!='CANCEL'&&
                              <th>{order.salesMan.phone}</th>
                              }
                              {this.type!='PENDING'&&this.type!='CANCEL'&&
                              <th>{order.offer.deliveryCost}</th>
                              }
                            </tr>
                            )}
                            
                          </table>
                          :
                          <div className='selectType'> 
                          <span >{allStrings.selectType}</span>
                          </div>
                          }
                          {this.state.selectFlag&& 
                          <div className='orderLoadMore'>
                            <button onClick={()=>{
                              this.getOrders(this.page+1);
                              this.page++;
                            }}>{allStrings.loadMore}</button>
                          </div>
                          }
                          </Modal>       
                    </div>
            </div>
        </div>
        </div>
        <Footer></Footer>
        </div>
    </div>
      );
    }
  }

  
  const mapToStateProps = state => ({
    isRTL: state.lang.isRTL,
    currentUser: state.auth.currentUser,
    select: state.menu.select,
  })
  
  const mapDispatchToProps = {
    getUser,
  }


export default connect(mapToStateProps,mapDispatchToProps) ( AdminsInfo = Form.create({ name: 'normal_login' })(AdminsInfo)) ;
