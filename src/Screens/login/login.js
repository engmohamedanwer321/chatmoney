import React from 'react';
import './login.css';
import {Icon,Button,Card,Input,CardTitle} from 'react-materialize';
import {Form,Checkbox,message} from 'antd'
import "antd/dist/antd.css";
import axios from 'axios';
import {BASE_END_POINT} from '../../config/URL'
import {withRouter} from 'react-router-dom'
import { connect } from 'react-redux';
import {login} from '../../actions/AuthActions'
import  {allStrings} from '../../assets/strings'

class Login extends React.Component {
    
    componentDidMount(){
       // console.log('Received values of form: ', this.props.userToken);
    }
    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
          if (!err) {
            console.log('Received values of form: ', values);
            this.props.login(values.username,values.password,this.props.userToken,this.props.history)
            
          }
        });
      }
    render() {
        const { getFieldDecorator } = this.props.form;
      return (
        <div class="container">
        <div className='login'>
        <div class="row">
            <div class="col m2">
                <div class='title' style={{backgroundColor:'#001529'}}>
                    <h2 class="center-align" style={{color:'white'}}>تسجيل الدخول</h2>
                </div>
                <div class="row row-form">
                    <form class="col s12">
                    <Form onSubmit={this.handleSubmit} className="login-form">
                    <row>
                        <Form.Item style={{marginTop:'50px'}}>
                        {getFieldDecorator('username', {
                            rules: [{ required: true, message: 'من فضلك ادخل اسم المستخدم' }],
                        })(
                            <Input style={{textAlign:'right'}} placeholder="اسم المستخدم" />
                        )}
                        </Form.Item>

                        <Form.Item style={{marginTop:'50px'}}>
                        {getFieldDecorator('password', {
                            rules: [{ required: true, message: 'من فضلك ادخل كلمة المرور ' }],
                        })(
                            <Input  type="password" style={{textAlign:'right'}} placeholder="كلمة المرور" />
                        )}
                        </Form.Item>
                    
                    </row>
                   
                        <Form.Item>
                  
                    <row>
                    <br></br>
                    <br></br>
                        <Button type="primary" htmlType="submit" className="login-form-button"style={{color:'white',fontWeight:'500', width:'100%',height:'50px',backgroundColor:'#001529'}}>
                           {allStrings.login}
                        </Button>
                    </row>
                        </Form.Item>
                    
                    </Form>
                       
                    </form>
                </div>
            </div>
        </div>
        </div>
    </div>
      );
    }
  }

  const mapToStateProps = state => ({
    isRTL: state.lang.isRTL,
    userToken: state.auth.userToken,
  })
  
  const mapDispatchToProps = {
    login,
  }


export default  withRouter(connect(mapToStateProps,mapDispatchToProps)(Login = Form.create({ name: 'normal_login' })(Login))) ;
