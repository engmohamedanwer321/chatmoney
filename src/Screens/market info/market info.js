import React from 'react';
import AppMenu from '../../components/menu/menu';
import Nav from '../../components/navbar/navbar';
import Footer from '../../components/footer/footer';

import './market info.css';
import {Carousel} from 'react-materialize';
import "antd/dist/antd.css";
 import axios from 'axios';
import {BASE_END_POINT} from '../../config/URL'
import { connect } from 'react-redux';
import  {allStrings} from '../../assets/strings'
import { Upload, message,Modal, Form, Icon, Input, Button,Select} from 'antd';

class MarketInfo extends React.Component {
    state = {
        modal1Visible: false,
         category:this.props.location.state.data,
         file: this.props.location.state.data.img,
         }

         constructor(props){
          super(props)
          if(this.props.isRTL){
            allStrings.setLanguage('ar')
          }else{
            allStrings.setLanguage('en')
          }
        }
        
         onChange = (e) => {
            this.setState({file:e.target.files[0]});
        }
        
         componentDidMount()
         {
             //console.log("ANWEr")
             //console.log(this.props.currentUser)
            
             //this.props.location.state.data
             //console.log(this.props.location.state.data)
         }
    
    deleteCategory = () => {
        let l = message.loading(allStrings.wait, 2.5)
        axios.delete(`${BASE_END_POINT}market/${this.state.category.id}`,{
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${this.props.currentUser.token}`
          },
        })
        .then(response=>{
            l.then(() => message.success(allStrings.deleteDone, 2.5))
            this.props.history.goBack()

        })
        .catch(error=>{
            console.log(error.response)
            l.then(() => message.error('Error', 2.5))
        })
     }

     handleSubmit = (e) => {
      e.preventDefault();
      this.props.form.validateFields((err, values) => {
        if (!err) {
          const data={
            name:values.name,
            arabicName:values.arabicName,
            price:values.price,
            option:"room background color"
            
          }
          
          let l = message.loading(allStrings.wait, 2.5)
          axios.put(`${BASE_END_POINT}market/${this.state.category.id}`,JSON.stringify(data),{
            headers: {
              'Content-Type': 'application/json',
              'Authorization': `Bearer ${this.props.currentUser.token}`
            },
          })
          .then(response=>{
              l.then(() => message.success(allStrings.updatedDone, 2.5))
              this.props.history.goBack()
          })
          .catch(error=>{
           //   console.log(error.response)
              l.then(() => message.error(error.response.data.errors[0].msg, 2.5))
          })
        }
      });
      
    }

      
      //modal
     
      setModal1Visible(modal1Visible) {
        this.setState({ modal1Visible });
      }
    render() {
        const { getFieldDecorator } = this.props.form;
         //select
         const Option = Select.Option;
         const {category} = this.state

         function handleChange(value) {
            //console.log(value); 
         }
         //end select
        //upload props
        const props = {
          name: 'file',
          action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
          headers: {
            authorization: 'authorization-text',
          },
          onChange(info) {
            if (info.file.status !== 'uploading') {
              //console.log(info.file, info.fileList);
            }
            if (info.file.status === 'done') {
              message.success(`${info.file.name} file uploaded successfully`);
            } else if (info.file.status === 'error') {
              message.error(`${info.file.name} file upload failed.`);
            }
          },
        };

        //end upload props
        const {select} = this.props;
      return (
          
        <div>
         <AppMenu height={'150%'} goTo={this.props.history}></AppMenu>
          <Nav></Nav>
          <div style={{marginRight:!select?'20.2%':'5.5%'}}>
        <div className='login'>
        <div class="row">
            <div class="col m2">
                <div class='title' style={{backgroundColor:'#001529'}} >
                    <h2 class="center-align" style={{color:'#fff'}}>{allStrings.marketInfo}</h2>
                </div>
                
                <div class="row" style={{marginTop:"0px"}}>
                    <form class="col s12">
                        <div class="row">
                            <div class="input-field col s6">
                            <input id="name" type="text" class="validate" disabled value={category.id}>
                            </input>
                            <label for="name" class="active">{allStrings.id}</label>
                            </div>

                            <div class="input-field col s6">
                            <input id="name" type="text" class="validate" disabled value={category.name}>
                            </input>
                            <label for="name" class="active">{allStrings.englishName}</label>
                            </div>

                        </div>

                        <div class="row">
                            <div class="input-field col s6">
                            <input id="name" type="text" class="validate" disabled value={category.arabicName}>
                            </input>
                            <label for="name" class="active">{allStrings.arabicName}</label>
                            </div>

                            <div class="input-field col s6">
                            <input id="name" type="text" class="validate" disabled value={category.price}>
                            </input>
                            <label for="name" class="active">{allStrings.price}</label>
                            </div>

                        </div>

                        
                       

      
                            <a class="waves-effect waves-light btn btn-large delete " onClick={this.deleteCategory}><i class="material-icons left spcial">delete</i>{allStrings.remove}</a>
                            <a class="waves-effect waves-light btn btn-large edit" onClick={() => this.setModal1Visible(true)}><i class="material-icons left spcial">edit</i>{allStrings.edit}</a>


                            <div>
                            <Modal
                                title={allStrings.edit}
                                visible={this.state.modal1Visible}
                                onOk={this.handleSubmit}
                                okText={allStrings.ok}
                                cancelText={allStrings.cancel}
                                onCancel={() => this.setModal1Visible(false)}
                              >
                              
                               <Form onSubmit={this.handleSubmit} className="login-form">
                               
                               <label for="name" class="lab">{allStrings.englishName}</label>
                                  <Form.Item>
                                  {getFieldDecorator('name', {
                                      rules: [{ required: true, message: 'Please enter rank english name' }],
                                      initialValue: category.name
                                  })(
                                      <Input/>
                                  )}
                                  </Form.Item>

                                  <label for="name" class="lab">{allStrings.arabicName}</label>
                                  <Form.Item>
                                  {getFieldDecorator('arabicName', {
                                      rules: [{ required: true, message: 'Please enter rank arabic name' }],
                                      initialValue: category.arabicName
                                  })(
                                      <Input/>
                                  )}
                                  </Form.Item>

                                  <label for="name" class="lab">{allStrings.price}</label>
                                  <Form.Item>
                                  {getFieldDecorator('price', {
                                      rules: [{ required: true, message: 'Please enter rank price' }],
                                      initialValue: category.price
                                  })(
                                      <Input/>
                                  )}
                                  </Form.Item>

                                 
                              </Form>
                            </Modal>

                                
                        
                            </div>
                        </form>
                        
                    </div>
            </div>
        </div>
        </div>
        <Footer></Footer>
        </div>
    </div>
      );
    }
  }

  const mapToStateProps = state => ({
    isRTL: state.lang.isRTL,
    currentUser: state.auth.currentUser,
    select: state.menu.select,
  })
  
  const mapDispatchToProps = {
  }

export default connect(mapToStateProps,mapDispatchToProps)(MarketInfo = Form.create({ name: 'normal_login', })(MarketInfo)) ;
