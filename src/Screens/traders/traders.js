
import React from 'react';
import AppMenu from '../../components/menu/menu';
import Nav from '../../components/navbar/navbar';
import Tables from '../../components/table/table';
import Footer from '../../components/footer/footer';

import './traders.css';
//import {Icon,Button,Modal,Input} from 'react-materialize';
import {Layout, Menu, DatePicker, Skeleton,Upload,Modal, Form, Icon, Input, Button,  Popconfirm, message,Select} from 'antd';
import "antd/dist/antd.css";
import axios from 'axios';
import {BASE_END_POINT} from '../../config/URL'
import {withRouter} from 'react-router-dom'
import { connect } from 'react-redux'
import  {allStrings} from '../../assets/strings'
//

class Traders extends React.Component {
 
    constructor(props){
      super(props)
      this.getUsers(1)
      if(this.props.isRTL){
        allStrings.setLanguage('ar')
      }else{
        allStrings.setLanguage('en')
      }
    }
   
    pagentationPage=0;
    counter=0;
    state = {
        modal1Visible: false,
        confirmDelete: false,
        selectedUser:null,
        users:[],
        loading:true
        }

      flag = -1;
       getUsers = (page,deleteRow) => {
         axios.get(`${BASE_END_POINT}getAll?type=TRADER&page=${page}&limit={20}`)
         .then(response=>{
           console.log("ALL users")
           console.log(response.data)
           this.setState({users:deleteRow?response.data.data:[...this.state.users,...response.data.data],loading:false})
         })
         .catch(error=>{
          // console.log("ALL users ERROR")
           //console.log(error.response)
         })
       }
     
       OKBUTTON = (e) => {
        this.deleteUser()
       }
 
       deleteUser = () => {
         let l = message.loading(allStrings.wait, 2.5)
         axios.delete(`${BASE_END_POINT}${this.state.selectedUser}/delete`,{
           headers: {
             'Content-Type': 'application/x-www-form-urlencoded',
             'Authorization': `Bearer ${this.props.currentUser.token}`
           },
         })
         .then(response=>{
             l.then(() => message.success(allStrings.deleteDone, 2.5))
             this.getUsers(1,true)
             this.flag = -1
         })
         .catch(error=>{
             //console.log(error.response)
             l.then(() => message.error('Error', 2.5))
         })
      }

    //submit form
    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
          if (!err) {
            //let date  = values.birthYear._d.toLocaleDateString().split('/')
            //console.log('date ', date[2]  );
            var data = new FormData();
            
            data.append("username",values.username)
            data.append("email",values.email)
            data.append("phone",values.phone)
            data.append("country",values.country)
            data.append("city",values.city)
            data.append("password",values.password)
            data.append("titleName",values.titleName)
            data.append("gender",values.gender.key)
            data.append("type",values.type.key)
            data.append("socialStatus",values.socialStatus.key)
            
            let l = message.loading(allStrings.wait, 2.5)
            axios.post(`${BASE_END_POINT}signup`,data,{
              headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${this.props.currentUser.token}`
              },
            })
            .then(response=>{
              console.log(allStrings.addDone)
                l.then(() => message.success('Add User', 2.5));
                this.setState({ modal1Visible:false });
                this.getUsers(1,true)
                this.flag = -1
                this.props.form.resetFields()
            })
            .catch(error=>{
              console.log("Add USer Error")
                console.log(error.response)
                l.then(() => message.error(error.response.data.errors[0].msg, 2.5))
            })

          }
        });
        
      }
    //end submit form
      


    showModal = () => {
      this.setState({
        visible: true,
      });
      
    }
    

    setModal1Visible(modal1Visible) {
      this.setState({ modal1Visible });
    }


    setModal2Visible(modal2Visible) {
      this.setState({ modal2Visible });
    }

    
    validatePhone = (rule, value, callback) => {
      const { form } = this.props;
      if ( isNaN(value) ) {
        callback('Please enter coorect phone');
      }else if ( value.length <11 ) {
        callback('Phone number must be grater than 11 digit');
      } 
      else {
        callback();
      }
    };
  
  
//end modal//

//end modal

    render() {
      const{select} = this.props
        //form
         const { getFieldDecorator } = this.props.form;
         //select
         const Option = Select.Option;

         function handleChange(value) {
            //console.log(value); 
         }
         //end select
         let controls = (
            <Popconfirm
            title={allStrings.areYouSure}
            onConfirm={this.OKBUTTON}
            onCancel={this.fCANCELBUTTON}
            okText={allStrings.yes}
            cancelText={allStrings.no}
          >
             <Icon className='controller-icon' type="delete" />
          </Popconfirm>
         )
        
        
         let list = this.state.users.map(val=>
          [val.id,val.username,val.phone,val.email,""+val.country,
         ""+val.city,val.socialStatus,val.balance,controls
        ])
         const loadingView = [
          [<Skeleton  active/> ],
          [<Skeleton active/> ],
          [<Skeleton  active/> ],
          [<Skeleton active/> ],
          
         ]
      return (
          <div>
               <AppMenu  height={'140%'} goTo={this.props.history}></AppMenu>
              <Nav></Nav>
              <div style={{marginRight:!select?'20.2%':'5.5%',borderWidth:2}}>
              <Tables 
              columns={this.state.loading?['Loading...']:[allStrings.id,allStrings.name, allStrings.phone,allStrings.email,allStrings.country,allStrings.city,allStrings.socialStatus,allStrings.balance,allStrings.remove]} title={allStrings.traders}
              arr={this.state.loading?loadingView:list}
              onCellClick={(colData,cellMeta)=>{
                //console.log('col index  '+cellMeta.colIndex)
                //console.log('row index   '+colData)
                if(cellMeta.colIndex!=8){
                  console.log(colData)
                  console.log('row   ',cellMeta.rowIndex+"         "+cellMeta.dataIndex)
                  this.props.history.push('/TradersInfo',{data:this.state.users[this.pagentationPage+cellMeta.rowIndex]})
                }else if(cellMeta.colIndex==8){
                    const id = list[ this.pagentationPage+cellMeta.rowIndex][0];
                    this.setState({selectedUser:id})
                    //console.log(id)
                  }
              }}

              
               onChangePage={(currentPage)=>{
                 if(currentPage>this.counter){
                   this.counter=currentPage;
                   this.pagentationPage=this.pagentationPage+10
                 }else{
                  this.counter=currentPage;
                  this.pagentationPage=this.pagentationPage-10
                 }
                console.log(currentPage)
                if(currentPage%2!=0  && currentPage > this.flag){
                  this.getUsers(currentPage+1)
                  this.flag  = currentPage;
                 
                }
                  
              }}
              ></Tables>
              <div>
              <Button style={{color: 'white', backgroundColor:'#25272e', marginLeft:60}}  onClick={() => this.setModal1Visible(true)}>{allStrings.addTrader}</Button>
              <Modal
                    title={allStrings.add}
                    visible={this.state.modal1Visible}
                    onOk={this.handleSubmit}
                    okText={allStrings.ok}
                    cancelText={allStrings.cancel}
                    onCancel={() => this.setModal1Visible(false)}
                  >
                    <Form onSubmit={this.handleSubmit} className="login-form">
                   
                    <Form.Item>
                    {getFieldDecorator('username', {
                        rules: [{ required: true, message: 'Please enter  Name' }],
                    })(
                        <Input placeholder={allStrings.name} />
                    )}
                    </Form.Item>
                                                        
                    <Form.Item>
                    {getFieldDecorator('email', {
                        rules: [
                          { required: true, message: 'Please enter email' },
                          {type: 'email',  message: 'Please enter correct email' }
                        ],
                    })(
                        <Input placeholder={allStrings.email} />
                    )}
                    </Form.Item>

                    <Form.Item hasFeedback >
                    {getFieldDecorator('phone', {
                        rules: [
                          {required: true, message: 'Please enter phone' },
                          {
                            validator: this.validatePhone,
                          },
                        ],
                        
                    })(
                        <Input placeholder={allStrings.phone} />
                    )}
                    </Form.Item>
                  
                    <Form.Item>
                    {getFieldDecorator('country', {
                        rules: [{ required: true, message: 'Please enter  country' }],
                    })(
                        <Input placeholder={allStrings.country} />
                    )}
                    </Form.Item>

                    <Form.Item>
                    {getFieldDecorator('city', {
                        rules: [{ required: true, message: 'Please enter  city' }],
                    })(
                        <Input placeholder={allStrings.city} />
                    )}
                    </Form.Item>

                    <Form.Item>
                    {getFieldDecorator('titleName', {
                        rules: [{ required: true, message: 'Please enter  title name' }],
                    })(
                        <Input placeholder={allStrings.titleName} />
                    )}
                    </Form.Item>
                   
                    <Form.Item>
                      {getFieldDecorator('gender', {
                        rules: [{ required: true, message: 'Please enter gender' }],
                    })(
                        <Select labelInValue  
                        placeholder={allStrings.gender}
                        style={{ width: '100%'}} onChange={handleChange}>
                            <Option value="male">{allStrings.male}</Option>
                            <Option value="female">{allStrings.female}</Option>
                        </Select>
                    )}
                    </Form.Item>

                    <Form.Item>
                      {getFieldDecorator('type', {
                        rules: [{ required: true, message: 'Please enter type' }],
                    })(
                        <Select labelInValue  
                        placeholder={allStrings.type}
                        style={{ width: '100%'}} onChange={handleChange}>
                            <Option value="CLIENT">{allStrings.user}</Option>
                            <Option value="TRADER">{allStrings.trader}</Option>
                            <Option value="ADMIN">{allStrings.admin}</Option>
                        </Select>
                    )}
                    </Form.Item>

                    <Form.Item>
                      {getFieldDecorator('socialStatus', {
                        rules: [{ required: true, message: 'Please enter social status' }],
                    })(
                        <Select labelInValue  
                        placeholder={allStrings.socialStatus}
                        style={{ width: '100%'}} onChange={handleChange}>
                            <Option value="single">{allStrings.single}</Option>
                            <Option value="married">{allStrings.married}</Option>
                        </Select>
                    )}
                    </Form.Item>

                    <Form.Item>
                    {getFieldDecorator('password', {
                        rules: [{ required: true, message: 'Please enter  title password' }],
                    })(
                        <Input placeholder={allStrings.password} />
                    )}
                    </Form.Item>
                   
                    </Form>
                </Modal>
              
              </div>
              </div>
          </div>
      );
    }
  }

  const mapToStateProps = state => ({
    isRTL: state.lang.isRTL,
    currentUser: state.auth.currentUser,
    select: state.menu.select,

  })
  
  const mapDispatchToProps = {
  }


  export default withRouter( connect(mapToStateProps,mapDispatchToProps)  (Traders = Form.create({ name: 'normal_login' })(Traders)) );
 

